
file: build.gradle
    compile 'com.neovisionaries:nv-websocket-client:1.30'
    compile files('libs/libjingle_peerconnection.jar')

TURN_URL="turn:203.190.170.131:45352";
STUN_URL="stun:203.190.170.131:45352";

Cách gọi

private void initTurnServer(){
    String dataJsonTurn = "{\n" +
                "    \"params\": {\n" +
                "        \"pc_config\": {\n" +
                "            \"iceServers\": [\n" +
                "                           {\n" +
                "                           \"username\": \"\",\n" +
                "                           \"password\": \"\",\n" +
                "                           \"urls\": [\n" +
                "                                    \"stun:203.190.170.131:45352\"\n" +
                "                                    ]\n" +
                "                           },\n" +
                "                           {\n" +
                "                           \"username\": \"viettel\",\n" +
                "                           \"password\": \"123456aA\",\n" +
                "                           \"urls\": [\n" +
                "                                    \"turn:203.190.170.131:45352\"\n" +
                "                                    ]\n" +
                "                           }\n" +
                "                           ]\n" +
                "        }\n" +
                "    },\n" +
                "    \"result\": \"SUCCESS\"\n" +
                "}\n";
    JSONObject appConfig = null;
    try {
      appConfig = new JSONObject(dataJsonTurn);
      String result = appConfig.getString("result");
      Log.i(TAG, "client debug ");
      if (!result.equals("SUCCESS")) {
        return;
      }

      String params = appConfig.getString("params");
      appConfig = new JSONObject(params);
      LinkedList<PeerConnection.IceServer> iceServers = WebSocketRTCClient.iceServersFromPCConfigJSON(appConfig.getString("pc_config"));
      AppRTCClient.SignalingParameters signalingParameters = new AppRTCClient.SignalingParameters(iceServers);
    } catch (JSONException e) {
      e.printStackTrace();
    }

  }
