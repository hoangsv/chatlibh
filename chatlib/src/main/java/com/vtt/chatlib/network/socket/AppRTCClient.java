package com.vtt.chatlib.network.socket;/*
 *  Copyright 2013 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

import com.vtt.chatlib.network.socket.model.EContactConversation;
import com.vtt.chatlib.network.socket.model.EContactCustomer;
import com.vtt.chatlib.network.socket.model.EContactMessage;
import com.vtt.chatlib.network.socket.model.EContactResponse;


/**
 * AppRTCClient is the interface representing an AppRTC client.
 */
public interface AppRTCClient {

  /**
   * Struct holding the connection parameters of an AppRTC room.
   */
  class RoomConnectionParameters {

    public String roomUrl;
    public String from;
    public boolean initiator;
    public String to;

    public RoomConnectionParameters(
        String roomUrl,
        String from,
        boolean initiator) {
      this.roomUrl = roomUrl;
      this.from = from;
      this.initiator = initiator;
    }

  }

  /**
   * Asynchronously connect to an AppRTC room URL using supplied connection
   * parameters. Once connection is established onConnectedToRoom()
   * callback with room parameters is invoked.
   */
  void connectToWebsocket(RoomConnectionParameters connectionParameters);

  /**
   * Send offer SDP to the other participant.
   */
//  void call(final SessionDescription sdp);
  /**
   * Send offer SDP to the other participant.
   */
  //public void sendOfferSdp(final SessionDescription sdp);

  /**
   * Send answer SDP to the other participant.
   */
//  void sendOfferSdp(final SessionDescription sdp, final boolean isScreensharing);

  /**
   * Send Ice candidate to the other participant.
   */
//  void sendLocalIceCandidate(final IceCandidate candidate, final boolean isScreensharing);

  /**
   * Disconnect from room.
   */
  void reconnect();

//  void register();

//  void login();

  void initUser();
//  void makeCall();
//  void joinRoom();

//  void sendEndVideoCall();

  /**
   * Send stop message to peer
   */
  void sendStopToPeer();

  /**
   * Disconnect from room.
   */
  void sendDisconnectToPeer();

//  void sendTextMessage(String messageText);

//  void leaveConversation();
//
//  void updateUserInfo(String username, String email);

  /**
   * Callback interface for messages delivered on signaling channel.
   * <p>
   * <p>Methods are guaranteed to be invoked on the UI thread of |activity|.
   */
  interface SignalingEvents {


    void updateChatBot(EContactResponse response);

    void updateChatBot(EContactConversation response);

    void updateChatBot(EContactMessage response);

    void updateChatBot(EContactCustomer response);

    void onConnected();

    void loginSuccess();

    void updateTyping();

    void onChannelClose();
  }
}