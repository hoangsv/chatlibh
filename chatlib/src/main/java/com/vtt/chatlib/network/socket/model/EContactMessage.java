package com.vtt.chatlib.network.socket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HaiKE on 10/30/17.
 */

public class EContactMessage {
  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("r")
  @Expose
  private String code;

  @SerializedName("needShow")
  @Expose
  private boolean needShow;

  public boolean isNeedShow() {
    return needShow;
  }

  public void setNeedShow(boolean needShow) {
    this.needShow = needShow;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
