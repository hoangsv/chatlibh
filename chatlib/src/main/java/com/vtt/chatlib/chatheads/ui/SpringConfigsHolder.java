package com.vtt.chatlib.chatheads.ui;


import com.facebook.rebound.SpringConfig;

/**
 * Created by kirankumar on 13/02/15.
 */
public class SpringConfigsHolder {
  public static final SpringConfig NOT_DRAGGING = SpringConfig.fromOrigamiTensionAndFriction(190, 20);
  public static final SpringConfig CAPTURING = SpringConfig.fromOrigamiTensionAndFriction(100, 10);
  public static final SpringConfig DRAGGING = SpringConfig.fromOrigamiTensionAndFriction(0, 1.5);
}
