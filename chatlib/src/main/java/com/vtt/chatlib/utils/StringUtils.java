package com.vtt.chatlib.utils;

/**
 * Created by phamh_000 on 11/12/2017.
 */

public class StringUtils {
  public static boolean isEmpty(final CharSequence cs) {
    return cs == null || cs.length() == 0;
  }

  public static boolean isNotEmpty(final CharSequence cs) {
    return !isEmpty(cs);
  }

  public static String checkNullString(String s) {
    return s == null ? "" : s;
  }

  public static boolean isEmptyNA(String s) {
    return s == null || s.trim().isEmpty() || "null".equalsIgnoreCase(s) || "n/a".equalsIgnoreCase(s)
        || "na".equalsIgnoreCase(s)
        || "-".equalsIgnoreCase(s);
  }
}
