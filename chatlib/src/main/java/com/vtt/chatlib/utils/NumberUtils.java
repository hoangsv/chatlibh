package com.vtt.chatlib.utils;

import android.content.Context;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by phamh_000 on 11/12/2017.
 */

public class NumberUtils {
  public static final float EPSILON = 0.00000001f;

  public static String formatPercentWithCheckEmpty(Context context, Float percent) {
    if (percent == null) {
      return "";
    } else if (percent == 0) {
      return "N/A";
    } else {
      String formatedString = getCommonFormat().format(percent);
      return String.format("%s%%", formatedString);
    }
  }

  public static synchronized DecimalFormat getCommonFormat() {
    return new DecimalFormat("#,###,###.##", getFormatSymbols());
  }

  private static DecimalFormatSymbols getFormatSymbols() {
    DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
    otherSymbols.setDecimalSeparator('.');
    otherSymbols.setGroupingSeparator(',');
    return otherSymbols;
  }

  public static String formatPercent(Float percent) {
    if (percent == null)
      return "";
    String formatedString = getCommonFormat().format(percent);
    return String.format("%s%%", formatedString);
  }

  public static String formatDelta(float detlta) {
    String formatedString = getCommonFormat().format(detlta);
    return formatedString;
  }

  public static boolean isZero(float num) {
    if (Math.abs(num - 0f) < EPSILON) {
      return true;
    }
    return false;
  }

  public static String formatValue(Float value) {
    if (value == null)
      return "";
    String yourFormattedString = getValueFormat().format(value);
    return yourFormattedString;
  }

  private static DecimalFormat sValueFormat;

  private static synchronized DecimalFormat getValueFormat() {
    if (sValueFormat == null) {
      sValueFormat = new DecimalFormat("#,###,###.##", getFormatSymbols());
    }

    return sValueFormat;
  }

}
