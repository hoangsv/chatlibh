package com.vtt.chatlib.chatdetail.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Hungpq on 12/28/2017.
 */

public class AvailableFeature {

  @SerializedName("title")
  private String title;
  @SerializedName("description")
  private String subTitle;
  @SerializedName("icon")
  private String iconUrl;
  @SerializedName("payload")
  private Object payload;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubTitle() {
    return subTitle;
  }

  public void setSubTitle(String subTitle) {
    this.subTitle = subTitle;
  }

  public String getIconUrl() {
    return iconUrl;
  }

  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(Object payload) {
    this.payload = payload;
  }

}
