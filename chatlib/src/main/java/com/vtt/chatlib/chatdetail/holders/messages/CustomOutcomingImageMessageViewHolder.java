package com.vtt.chatlib.chatdetail.holders.messages;

import android.view.View;

import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.chatkit.messages.MessageHolders;

/*
 * Created by troy379 on 05.04.17.
 */
public class CustomOutcomingImageMessageViewHolder
    extends MessageHolders.OutcomingImageMessageViewHolder<Message> {

  public CustomOutcomingImageMessageViewHolder(View itemView) {
    super(itemView);
  }

  @Override
  public void onBind(Message message) {
    super.onBind(message);

    time.setText(message.getStatus() + " " + time.getText());
  }
}