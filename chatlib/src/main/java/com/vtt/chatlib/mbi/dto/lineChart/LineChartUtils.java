package com.vtt.chatlib.mbi.dto.lineChart;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.Transformer;
import com.vtt.chatlib.R;
import com.vtt.chatlib.mbi.dto.common.Charts;
import com.vtt.chatlib.mbi.dto.common.GemLargeValueFormatter;
import com.vtt.chatlib.mbi.dto.common.ItemLegend;
import com.vtt.chatlib.mbi.dto.common.LegendsAdapter;
import com.vtt.chatlib.mbi.dto.common.LineLongClickListener;
import com.vtt.chatlib.mbi.dto.common.PopOverChartModel;
import com.vtt.chatlib.mbi.uicomponent.ChartPopOver;
import com.vtt.chatlib.utils.NumberUtils;

import java.util.List;

/**
 * Created by Hungpq on 12/13/2017.
 */

public class LineChartUtils {
  public static void bindServiceDataAreaChart(LineChart mLineChart, LineData data, List<String> dates,
                                              List<ItemLegend> listLegends, RecyclerView mLegendRv,
                                              LegendsAdapter.OnItemLegendClickListener onItemLegendClickListener) {
    Charts.initLineChart(mLineChart.getContext(), mLineChart);
    mLineChart.setExtraBottomOffset(10);
    mLineChart.resetTracking();
    mLineChart.clear();
    if (data == null || dates == null || dates.isEmpty()) {
      return;
    }
    // x axis
    XAxis xAxis = mLineChart.getXAxis();
    xAxis.setDrawLabels(true);
    xAxis.setLabelRotationAngle(-80);

    Charts.configureChartYAxisLabels(mLineChart, dates);

    int max = dates.size();

    xAxis.setAxisMinimum(0f);
    xAxis.setAxisMaximum(max - 1f);
//    xAxis.setLabelCount(max, true);
    xAxis.setCenterAxisLabels(false);
//    mLineChart.setVisibleXRangeMaximum(max);

    xAxis.setGranularityEnabled(true);
    xAxis.setGranularity(1.0f);
    xAxis.setTextSize(8f);
    if (data.getYMax() > 1000)
      mLineChart.getAxisLeft().setValueFormatter(new GemLargeValueFormatter());
    // Y axis
//    mLineChart.getAxisLeft().setValueFormatter(new IAxisValueFormatter() {
//
//      @Override
//      public String getFormattedValue(float value, AxisBase axis) {
//        return NumberUtils.getCommonFormat().format(value);
//      }
//    });

    // create a data object with the datasets
    data.setValueTextSize(9f);
    data.setDrawValues(false);
    data.setHighlightEnabled(true);
    // set data
    mLineChart.setData(data);
//    if (data.getYMin() >= 0f) {
//      mLineChart.getAxisLeft().setAxisMinimum(0f);
//    } else {
//      mLineChart.getAxisLeft().setAxisMinimum(data.getYMin());
//    }
//
// mLineChart.setVisibleXRangeMaximum(Constants.CHART.MAX_VISIBLE_ENTRY);
    YAxis left = mLineChart.getAxisLeft();
    if (NumberUtils.isZero(data.getYMin()) && NumberUtils.isZero(data.getYMax())) {
      left.setAxisMaximum(1f);
      left.setAxisMinimum(0f);
      left.setLabelCount(2, true);
    } else {
//      int step = (int) ((mLineChart.getYMax() - mLineChart.getYMin()) / (mLineChart.getAxisLeft().getLabelCount() - 1));
//      if (data.getYMin() < 0)
//        mLineChart.getAxisLeft().setAxisMinimum(data.getYMin() - step);
//      else mLineChart.getAxisLeft().setAxisMinimum(0f);
//      mLineChart.getAxisLeft().setAxisMaximum(data.getYMax() + step);
      if (data.getYMin() > 0) {
        mLineChart.getAxisLeft().setAxisMinimum(0f);
      } else mLineChart.getAxisLeft().setAxisMinimum(data.getYMin());
    }
    mLineChart.invalidate();

    // bind legends :
    if (mLegendRv != null) {
      Charts.setupLegends(mLegendRv, listLegends, onItemLegendClickListener);
    }

//    mLineChart.setMarker(null);
    mLineChart.getXAxis().setAxisMaximum(mLineChart.getXAxis().getAxisMaximum() + 0.5f);
    if (max == 1) {
      mLineChart.getXAxis().setLabelCount(6);
    }

    YAxis yAxis = mLineChart.getAxisLeft();
    if (mLineChart.getData().getYMax() < 10) {
      left.setAxisMaximum(data.getYMax());
      if (NumberUtils.isZero(mLineChart.getData().getYMax() - 1.0f))
        yAxis.setLabelCount((int) data.getYMax(), true);
      else
        yAxis.setLabelCount((int) data.getYMax() + 1, true);

    }

//    float minimum = mLineChart.getAxisLeft().getAxisMinimum();
//    float maximum = mLineChart.getAxisLeft().getAxisMaximum();
//    mLineChart.getAxisLeft().calculate(minimum, maximum);
//    float range = mLineChart.getAxisLeft().mAxisRange;
//
//    maximum = ((int) maximum / range + 1) * range;
//    mLineChart.getAxisLeft().setAxisMaximum(maximum);
//    mLineChart.getAxisLeft().calculate(minimum, maximum);
    //==========================
//    float[] mEntries = mLineChart.getAxisLeft().mEntries;
//    for (int i = 0; i < mEntries.length; i++) {
//      mEntries[i] = Math.round(mEntries[i]);
//    }
//    mLineChart.getAxisLeft().mEntries = mEntries;
//
//    mLineChart.invalidate();
//    Bitmap starBitmap = BitmapFactory.decodeResource(mLineChart.getContext().getResources(), R.drawable.ic_id_staff);
//    mLineChart.setRenderer(new ImageLineChartRenderer(mLineChart, mLineChart.getAnimator(), mLineChart.getViewPortHandler(), starBitmap));
//    mLineChart.invalidate();
  }

  public static void setupActionHoldLineChart(final LineChart lineChart, final RelativeLayout containerView,
                                              final IEntryPoupOverSearchable data) {
    final LinearLayout mLineChartCoverll = new LinearLayout(containerView.getContext());
    containerView.addView(mLineChartCoverll);
    mLineChartCoverll.setVisibility(View.GONE);
    LineLongClickListener listener = new LineLongClickListener();
    listener.setListener(new LineLongClickListener.LongPressListener() {
      @Override
      public void onLongPress(MotionEvent me) {
        showHighlight(lineChart, containerView, data, mLineChartCoverll, me, true);
      }

      @Override
      public boolean onLongPressMove(MotionEvent me) {
        if (mLineChartCoverll.getVisibility() == View.VISIBLE) {
          mLineChartCoverll.requestDisallowInterceptTouchEvent(true);
          mLineChartCoverll.getParent().requestDisallowInterceptTouchEvent(true);
          LineChartUtils.updatePopOver(lineChart, containerView, data, mLineChartCoverll, me, true);
          return true;
        }

        return false;
      }

      @Override
      public void onEndLongPress(MotionEvent me) {
        mLineChartCoverll.setVisibility(View.GONE);
        ChartPopOver.dissmiss();
      }
    });
    lineChart.setOnChartGestureListener(listener);
    lineChart.setOnTouchListener(listener);
  }

  public static void showHighlight(LineChart lineChart, final RelativeLayout containerView, IEntryPoupOverSearchable data,
                                   final LinearLayout lineChartCoverLayout, MotionEvent me, boolean showPopOver) {
    LineChartUtils.getCoverGroupLineChart(lineChart, me, lineChartCoverLayout);
    Entry entry = lineChart.getEntryByTouchPoint(me.getX(), me.getY());
    if (entry == null) {
      return;
    }

    final PopOverChartModel detaillModel = data.getPopOverModel(Math.round(entry.getX()));
    lineChartCoverLayout.setVisibility(View.VISIBLE);

    if (showPopOver) {
      lineChartCoverLayout.post(new Runnable() {
        @Override
        public void run() {
          if (detaillModel != null) {
            ChartPopOver.showDialogDetailInfo(containerView.getContext(),
                detaillModel, lineChartCoverLayout, 0, -1);
          }
        }
      });
    }
    Log.e("######", "end");
  }

  public static void updatePopOver(LineChart lineChart, final RelativeLayout containerView, IEntryPoupOverSearchable data,
                                   final LinearLayout lineChartCoverLayout, MotionEvent me, boolean showPopOver) {
    Entry entry = lineChart.getEntryByTouchPoint(me.getX(), me.getY());

    if (entry == null) return;

    final PopOverChartModel detaillModel = data.getPopOverModel((int) entry.getX());

    if (detaillModel == null) return;


    LineChartUtils.getCoverGroupLineChart(lineChart, me, lineChartCoverLayout);

    ChartPopOver.updateDialogData(containerView.getContext(),
        detaillModel, lineChartCoverLayout);
  }

  public static void getCoverGroupLineChart(LineChart mLineChart, MotionEvent me, LinearLayout mCoverView) {
    Entry entry = mLineChart.getEntryByTouchPoint(me.getX(), me.getY());
    caculateCover(mLineChart, entry, mCoverView);

  }

  static int LineChartHeight;

  public static void caculateCover(LineChart mLineChart, Entry entry, LinearLayout mCoverView) {
    if (mCoverView.getHeight() == 0)
      LineChartHeight = LineChartUtils.getLineHeightInPixel(mLineChart);
    else
      LineChartHeight = mCoverView.getHeight();

    int groupWidth = getGroupWidth(mLineChart);

    final int maxWidth = mCoverView.getContext().getResources()
        .getDimensionPixelOffset(R.dimen.max_highlight_line_chart);

    if (groupWidth > maxWidth) {
      groupWidth = maxWidth;
    }
    int idex = entry == null ? -1 : Math.round(entry.getX());
    if (idex != -1) {
      mCoverView.setX(LineChartUtils.percentToPixcel(mLineChart, idex));
    } else {
      mCoverView.setX(LineChartUtils.percentToPixcel(mLineChart, 0));
    }
    // center highlight view acrossding value line
    mCoverView.setX(mCoverView.getX() - groupWidth / 2);
    mCoverView.setY(mLineChart.getY());

    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(groupWidth, LineChartHeight);
    mCoverView.setLayoutParams(params);
    mCoverView.setBackgroundColor(Color.parseColor("#25000000"));
  }

  public static int getGroupWidth(LineChart mLinechart) {
    return percentToPixcel(mLinechart, 2f) - percentToPixcel(mLinechart, 1f);
  }

  public static int getLineHeightInPixel(LineChart mLinechart) {
    MPPointD point = mLinechart.getTransformer(YAxis.AxisDependency.LEFT)
        .getPixelForValues(mLinechart.getXAxis().getAxisMinimum(), mLinechart.getAxisLeft().getAxisMinimum());
    return (int) point.y;
  }

  public static int percentToPixcel(LineChart mLinechart, float percent) {
    Transformer transformer = mLinechart.getTransformer(YAxis.AxisDependency.LEFT);
    MPPointD LineDimensPoint = transformer.getPixelForValues(percent, 0);
    return (int) LineDimensPoint.x;
  }

}
