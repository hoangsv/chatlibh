package com.vtt.chatlib.mbi.dto.horizontalChart;


/**
 * Created by bavv on 5/18/17.
 */

public class ItemHorizontalStackRankingUnitData {

  private boolean asc;
  private ItemHorizontalStackRankingUnitChartData chartData;

  public boolean isAsc() {
    return asc;
  }

  public void setAsc(boolean asc) {
    this.asc = asc;
  }

  public ItemHorizontalStackRankingUnitChartData getChartData() {
    return chartData;
  }

  public void setChartData(ItemHorizontalStackRankingUnitChartData chartData) {
    this.chartData = chartData;
  }
}
