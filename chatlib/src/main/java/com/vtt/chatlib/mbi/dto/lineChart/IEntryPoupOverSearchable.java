package com.vtt.chatlib.mbi.dto.lineChart;

import com.github.mikephil.charting.data.Entry;
import com.vtt.chatlib.mbi.dto.common.PopOverChartModel;

/**
 * IEntryPoupOverSearchable
 * Created by neo on 5/5/17.
 */
public interface IEntryPoupOverSearchable {
  PopOverChartModel getPopOverModel(Entry entry);

  PopOverChartModel getPopOverModel(int index);
}
