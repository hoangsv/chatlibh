package com.vtt.chatlib.mbi.dto.horizontalChart;


import com.vtt.chatlib.utils.NumberUtils;

import java.io.Serializable;

/**
 * Created by Thanhnc221 on 3/31/2017.
 */
public class DataColumnPiece implements Serializable {

  private Float value;
  private Float percent;
  private Integer styleId;
  private Float drawValue;

  public DataColumnPiece() {
  }

  public DataColumnPiece(Float value, Float percent, Integer styleId) {
    this.value = value;
    this.percent = percent;
    this.styleId = styleId;
  }

  public Float getValue() {
    return value;
  }

  public void setValue(Float value) {
    this.value = value;
  }

  public Float getPercent() {
    return percent == null ? 0 : percent;
//    return percent;
  }

  public String getPercentString() {
    return percent == null ? "N/A" : NumberUtils.formatPercent(percent);
  }

  public void setPercent(Float percent) {
    this.percent = percent;
  }

  public Integer getStyleId() {
    return styleId;
  }

  public void setStyleId(Integer styleId) {
    this.styleId = styleId;
  }

  public Float getDrawValue() {
    return drawValue;
  }

  public DataColumnPiece setDrawValue(Float drawValue) {
    this.drawValue = drawValue;
    return this;
  }
}
