package com.vtt.chatlib.mbi.dto.common;

import android.graphics.Color;

import com.vtt.chatlib.mbi.dto.lineChart.GroupColumnPieceStyle;


/**
 * Created by Administrator on 4/7/2017.
 */

public class ItemLegend {
  private String mTitle;
  private String mDesc;
  private int mColor;
  String percent;
  String viewValue;
  private boolean hiden;

  LegendType legendType;

  // for click legends events
  private String objectId;
  private String objectType;

  public ItemLegend(String mTitle, String mDesc) {
    this.mTitle = mTitle;
    this.mDesc = mDesc;
  }

  public ItemLegend(String mTitle, int mColor) {
    this.mTitle = mTitle;
    this.mColor = mColor;
  }

  public ItemLegend(String mTitle, int mColor, String percent, String viewValue) {
    this.mTitle = mTitle;
    this.mColor = mColor;
    this.percent = percent;
    this.viewValue = viewValue;
  }

  public ItemLegend(GroupColumnPieceStyle style) {
    this(style.getTitle(), null == style.getColor() ? Color.BLACK : Color.parseColor(style.getColor()));
    if (style.getObjectId() != null)
      this.objectId = style.getObjectId().toString();
  }

  public String getTitle() {
    return mTitle;
  }

  public ItemLegend setTitle(String title) {
    mTitle = title;
    return this;
  }

  public String getDesc() {
    return mDesc;
  }

  public ItemLegend setDesc(String desc) {
    mDesc = desc;
    return this;
  }

  public int getColor() {
    return mColor;
  }

  public ItemLegend setColor(int color) {
    mColor = color;
    return this;
  }

  public String getmTitle() {
    return mTitle;
  }

  public ItemLegend setmTitle(String mTitle) {
    this.mTitle = mTitle;
    return this;
  }

  public String getmDesc() {
    return mDesc;
  }

  public ItemLegend setmDesc(String mDesc) {
    this.mDesc = mDesc;
    return this;
  }

  public int getmColor() {
    return mColor;
  }

  public ItemLegend setmColor(int mColor) {
    this.mColor = mColor;
    return this;
  }

  public String getPercent() {
    return percent;
  }

  public ItemLegend setPercent(String percent) {
    this.percent = percent;
    return this;
  }

  public String getObjectId() {
    return objectId;
  }

  public ItemLegend setObjectId(String objectId) {
    this.objectId = objectId;
    return this;
  }

  public String getObjectType() {
    return objectType;
  }

  public ItemLegend setObjectType(String objectType) {
    this.objectType = objectType;
    return this;
  }

  public String getViewValue() {
    return viewValue;
  }

  public ItemLegend setViewValue(String viewValue) {
    this.viewValue = viewValue;
    return this;
  }

  public LegendType getLegendType() {
    return legendType;
  }

  public ItemLegend setLegendType(LegendType legendType) {
    this.legendType = legendType;
    return this;
  }

  public boolean isHiden() {
    return hiden;
  }

  public ItemLegend setHiden(boolean hiden) {
    this.hiden = hiden;
    return this;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ItemLegend) {
      if (((ItemLegend) obj).getTitle() != null && ((ItemLegend) obj).getLegendType() != null
          && getTitle() != null && getLegendType() != null) {
        if (((ItemLegend) obj).getTitle().equals(getTitle())
            && ((ItemLegend) obj).getLegendType().toString().equals(getLegendType().toString())) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
