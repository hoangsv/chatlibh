package com.vtt.chatlib.mbi.dto.lineChart;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * AccDivPlanChartLine DTO
 * Created by NEO on 2/22/2017.
 */
public class LChartLine {
  @SerializedName("title")
  private String title;

  @SerializedName("info")
  private List<LChartLineInfo> infoList;

  @SerializedName("color")
  private String color;

  @SerializedName("points")
  private List<LChartLinePoint> points;
  @SerializedName("average")
  private boolean average;

  public String getTitle() {
    return title;
  }

  public LChartLine setTitle(String title) {
    this.title = title;
    return this;
  }

  public List<LChartLineInfo> getInfoList() {
    return infoList;
  }

  public LChartLine setInfoList(List<LChartLineInfo> infoList) {
    this.infoList = infoList;
    return this;
  }

  public String getColor() {
    return color;
  }

  public LChartLine setColor(String color) {
    this.color = color;
    return this;
  }

  public List<LChartLinePoint> getPoints() {
    return points;
  }

  public LChartLine setPoints(List<LChartLinePoint> points) {
    this.points = points;
    return this;
  }

  public boolean isAverage() {
    return average;
  }

  public LChartLine setAverage(boolean average) {
    this.average = average;
    return this;
  }
}
