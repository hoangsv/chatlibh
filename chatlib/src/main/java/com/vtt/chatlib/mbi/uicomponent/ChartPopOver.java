package com.vtt.chatlib.mbi.uicomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.mbi.dto.common.ItemLegend;
import com.vtt.chatlib.mbi.dto.common.LegendsAdapter;
import com.vtt.chatlib.mbi.dto.common.PopOverChartModel;
import com.vtt.chatlib.mbi.dto.horizontalChart.DataColumnPiece;
import com.vtt.chatlib.mbi.dto.horizontalChart.GroupColumn;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitChartData;
import com.vtt.chatlib.mbi.dto.lineChart.GroupColumnPieceStyle;
import com.vtt.chatlib.utils.EasyDialog;
import com.vtt.chatlib.utils.NumberUtils;
import com.vtt.chatlib.utils.RecyclerUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 4/8/2017.
 */

public class ChartPopOver {
  private ChartPopOver() {
  }

  static PopupWindow mPopupWindow;
  static EasyDialog infoDialog = null;

  public static void dissmiss() {
    if (infoDialog != null) {
      infoDialog.dismiss();
    }
  }

  public static void showDialogDetailInfo(Context context, PopOverChartModel model, View anchor, int minLegends, int maxLegends) {
    bindDialogData(context, model, anchor, minLegends, maxLegends);
//    if (infoDialog.getDialog().getWindow().getAttributes().y < 0) {
//      Log.e("##############", infoDialog.getDialog().getWindow().getAttributes().y + "");
//    }
//    infoDialog.getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
//      @Override
//      public void onShow(DialogInterface dialog) {
//        int[] location = new int[2];
//        infoDialog.getDialog().findViewById(R.id.dialog_chart_name_tv).getLocationInWindow(location);
//        if (location[1] < 10) {
//          infoDialog.getDialog().getWindow().getAttributes().y = 0;
//          infoDialog.setLocation(new int[]{0, 0});
//        }
//      }
//    });

    if (null != infoDialog) {
      infoDialog.showAlert(false);
    }
  }

  public static void showHorizontalStackRankingUnitDialog(Context context, View anchor,
                                                          String chartTitle, ItemHorizontalStackRankingUnitChartData data, int indexData) {

    PopOverChartModel model = getPopOverModeHorizontalBarChart(chartTitle, data, indexData);
    bindDialogData(context, model, anchor, 2, -1);

    if (null != infoDialog)
      infoDialog.showAlert(false);
  }

  public static PopOverChartModel getPopOverModeHorizontalBarChart(String chartTitle,
                                                                   ItemHorizontalStackRankingUnitChartData data,
                                                                   int indexData) {
    PopOverChartModel popOverChartModel = new PopOverChartModel();
    popOverChartModel.setTitle(chartTitle);

    if (null == data) return popOverChartModel;

    List<GroupColumn> groupColumns = data.getColumns();
    if (null == groupColumns || groupColumns.isEmpty()) return null;
    if (indexData >= groupColumns.size())
      return popOverChartModel;

    GroupColumn groupColumn = groupColumns.get(indexData);
    if (null == groupColumn) return null;

    String valueStr = groupColumn.getValue() == null ? "N/A"
        : NumberUtils.formatValue(groupColumn.getValue());
    String percentStr = groupColumn.getPercent() == null ? "N/A"
        : NumberUtils.formatPercent(groupColumn.getPercent());

    String columnValue = groupColumn.getLabel()
        + " : "
        + valueStr
        + " ("
        + percentStr + ")";
    popOverChartModel.setColumnValue(columnValue);
    List<ItemLegend> itemLegends = new ArrayList<>();
    List<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
    HashMap<Integer, GroupColumnPieceStyle> styles = data.getMapStylePeice();

    if (null == dataColumnPieces || dataColumnPieces.isEmpty() || null == styles) return null;

    for (DataColumnPiece dc : dataColumnPieces) {
      ItemLegend il = new ItemLegend(styles.get(dc.getStyleId()));
      String viewValue = dc.getValue() == null ? "" : NumberUtils.formatValue(dc.getValue());
      String percent = dc.getPercent() == null ? "" : NumberUtils.formatPercent(dc.getPercent());
      il.setViewValue(viewValue);
      il.setPercent(percent);
      itemLegends.add(il);
    }

    popOverChartModel.setItemLegends(itemLegends);
    return popOverChartModel;
  }

  public static synchronized void bindDialogData(final Context context, PopOverChartModel model, View anchor, int minLegends, int maxLegends) {
//    if (null == model) return;

    final View layout = LayoutInflater.from(context).inflate(R.layout.dialog_top_chart_chat, null);
    TextView mNameTv = (TextView) layout.findViewById(R.id.dialog_chart_name_tv);
    TextView mColumValueTv = (TextView) layout.findViewById(R.id.dialog_chart_value_tv);
    RecyclerView mLegendValueRv = (RecyclerView) layout.findViewById(R.id.dialog_chart_list_ct_rv);
    bindDataInfo(mNameTv, mColumValueTv, mLegendValueRv, model, minLegends, maxLegends);

    infoDialog = new EasyDialog(context)
        .setLayout(layout)
        .setBackgroundColor(Color.WHITE)
        .setOutsideColor(ContextCompat.getColor(context, R.color.out_side_dialog_bg))
        .setTouchOutsideDismiss(true)
        .setMatchParent(true)
        .setLocationByAttachedView(anchor)
        .setMarginLeftAndRight(0, 0);
    infoDialog.setGravity(EasyDialog.GRAVITY_TOP);
//    int[] anchorLocation = new int[2];
//    anchor.getLocationOnScreen(anchorLocation);
//    int spaceBottom = Math.abs(DeviceUtils.getDeviceSize(context).y - anchor.getHeight() - anchorLocation[1]);
//    if (spaceBottom > anchorLocation[1]) {
//      if (model.getItemLegends().size() < 3)
//        infoDialog.setGravity(EasyDialog.GRAVITY_TOP);
//      else
//        infoDialog.setGravity(EasyDialog.GRAVITY_BOTTOM);
//    } else {
//      infoDialog.setGravity(EasyDialog.GRAVITY_TOP);
//    }
  }

  public static synchronized void updateDialogData(Context context, PopOverChartModel model, View anchor) {
    if (infoDialog == null || null == model) {
      return;
    }
    infoDialog.setLocationByAttachedView(anchor);
    View layout = infoDialog.getTipViewInstance();
    TextView mNameTv = (TextView) layout.findViewById(R.id.dialog_chart_name_tv);
    TextView mColumValueTv = (TextView) layout.findViewById(R.id.dialog_chart_value_tv);
    RecyclerView mLegendValueRv = (RecyclerView) layout.findViewById(R.id.dialog_chart_list_ct_rv);
    if (mNameTv == null) return;
    bindDataInfo(mNameTv, mColumValueTv, mLegendValueRv, model, 0, -1);
    infoDialog.setGravity(EasyDialog.GRAVITY_TOP);
  }

  public static void bindDataInfo(TextView mNameTv, TextView mGroupNameTv,
                                  final RecyclerView mLegendValueRv,
                                  PopOverChartModel model, int minLegendCount,
                                  final int maxLegendVisible) {
    if (model == null) {
      mNameTv.setText("");
      mGroupNameTv.setText("");
      LegendsAdapter adapter = (LegendsAdapter) mLegendValueRv.getAdapter();
      if (adapter != null)
        adapter.reset(new ArrayList<ItemLegend>());
      return;
    }
    mNameTv.setText(model.getTitle());
    if (model.getColumnValue() != null && !("".equalsIgnoreCase(model.getColumnValue()))) {
      mGroupNameTv.setText(model.getColumnValue());
      mGroupNameTv.setVisibility(View.VISIBLE);
    } else {
      mGroupNameTv.setVisibility(View.GONE);
    }
    // list value pieces
    List<ItemLegend> itemLegends = model.getItemLegends();
    // just show legends when legends count >=2
    if (itemLegends.size() >= minLegendCount) {
      if (mLegendValueRv.getAdapter() != null) {
        LegendsAdapter adapter = (LegendsAdapter) mLegendValueRv.getAdapter();
        adapter.setShowPercent(!model.isHidePercent()).setShowValue(!model.isHideValue());
        adapter.reset(itemLegends);
        adapter.notifyDataSetChanged();
        mLegendValueRv.post(new Runnable() {
          @Override
          public void run() {
            final int oneItemHigh = (int) (new Button(mLegendValueRv.getContext()).getTextSize()
                + 2 * mLegendValueRv.getContext().getResources().getDimensionPixelSize(R.dimen.padding_small));
            mLegendValueRv.post(new Runnable() {
              @Override
              public void run() {
                if (maxLegendVisible != -1 && mLegendValueRv.getHeight() > maxLegendVisible * oneItemHigh) {
                  mLegendValueRv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                      maxLegendVisible * oneItemHigh));
                }
              }
            });
//            }
          }
        });
      } else {
        RecyclerUtils.setupVerticalRecyclerView(mNameTv.getContext(), mLegendValueRv);
        final LegendsAdapter adapter =
            new LegendsAdapter(itemLegends, true).setShowPercent(!model.isHidePercent())
                .setShowValue(!model.isHideValue());
        mLegendValueRv.setAdapter(adapter);
        mLegendValueRv.post(new Runnable() {
          @Override
          public void run() {
            final int oneItemHigh = (int) (new Button(mLegendValueRv.getContext()).getTextSize()
                + 2 * mLegendValueRv.getContext().getResources().getDimensionPixelSize(R.dimen.padding_small));
//            View item = mLegendValueRv.getLayoutManager().getChildAt(0);
//            if (item != null) {
//              int oneItemHeight = item.getHeight();
            mLegendValueRv.post(new Runnable() {
              @Override
              public void run() {
                if (maxLegendVisible != -1 && mLegendValueRv.getHeight() > maxLegendVisible * oneItemHigh) {
                  mLegendValueRv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                      maxLegendVisible * oneItemHigh));
                }
              }
            });
//            }
          }
        });
      }
    }
  }

}
