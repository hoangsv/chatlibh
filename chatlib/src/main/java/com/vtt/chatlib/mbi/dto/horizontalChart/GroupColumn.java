package com.vtt.chatlib.mbi.dto.horizontalChart;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Thanhnc221 on 3/31/2017.
 */

public class GroupColumn implements Serializable {
  private String label;
  private Float value;
  private Float percent;


  private ArrayList<DataColumnPiece> dataColumnPieces;
  @SerializedName("event")
  private String event;


  public ArrayList<DataColumnPiece> getDataColumnPieces() {
    return dataColumnPieces;
  }

  public void setDataColumnPieces(ArrayList<DataColumnPiece> dataColumnPieces) {
    this.dataColumnPieces = dataColumnPieces;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Float getValue() {
    return value;
  }

  public void setValue(Float value) {
    this.value = value;
  }

  public Float getPercent() {
    return percent;
  }

  public void setPercent(Float percent) {
    this.percent = percent;
  }


  public String getEvent() {
    return event;
  }

  public String getEventText() {
    return (event == null || event.isEmpty()) ? "" : (" - " + event);
  }

  public void setEvent(String event) {
    this.event = event;
  }
}
