package com.vtt.chatlib.mbi.dto.horizontalChart;

import android.graphics.Color;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.common.primitives.Floats;
import com.vtt.chatlib.mbi.dto.common.ItemLegend;
import com.vtt.chatlib.mbi.dto.common.LegendType;
import com.vtt.chatlib.mbi.dto.common.PopOverChartModel;
import com.vtt.chatlib.mbi.dto.lineChart.GroupColumnPieceStyle;
import com.vtt.chatlib.utils.Logger;
import com.vtt.chatlib.utils.NumberUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Thanhnc221 on 4/3/2017.
 */
public class ItemHorizontalStackRankingUnitChartData {
  private static final String TAG = ItemHorizontalStackRankingUnitChartData.class.toString();
  private String chartName;
  private boolean viewMore;
  private String unit;
  private String objectType;

  private ArrayList<GroupColumnPieceStyle> columnPieceStyles;
  private ArrayList<GroupColumn> columns;

  private Map<Integer, GroupColumnPieceStyle> columnPieceStylesMap;
  private BarData mBarDataValue;
  private BarData mBarDataPercent;
  private List<String> labels;

  public float getMinPercent() {
    if (columns.size() == 0) return 0;
    Float min = columns.get(0).getPercent();
    for (GroupColumn gc : columns) {
      if (null == min)
        min = gc.getPercent();
      else {
        if (gc.getPercent() != null && gc.getPercent() < min)
          min = gc.getPercent();
      }
    }
    return null == min ? 0 : min;
  }

  public float getMaxPercent() {
    if (columns.size() == 0) return 0;
    Float max = columns.get(0).getPercent();
    for (GroupColumn gc : columns) {
      if (null == max) {
        max = gc.getPercent();
      } else {
        if (gc.getPercent() != null && gc.getPercent() > max)
          max = gc.getPercent();
      }
    }
    return null == max ? 0 : max;
  }


  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public String getChartName() {
    return chartName;
  }

  public void setChartName(String chartName) {
    this.chartName = chartName;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public ArrayList<GroupColumnPieceStyle> getColumnPieceStyles() {
    return columnPieceStyles;
  }

  public void setColumnPieceStyles(ArrayList<GroupColumnPieceStyle> columnPieceStyles) {
    this.columnPieceStyles = columnPieceStyles;
  }

  public ArrayList<GroupColumn> getColumns() {
    return columns;
  }

  public void setColumns(ArrayList<GroupColumn> columns) {
    this.columns = columns;
  }

  public boolean isViewMore() {
    return viewMore;
  }

  public ItemHorizontalStackRankingUnitChartData setViewMore(boolean viewMore) {
    this.viewMore = viewMore;
    return this;
  }

  public void resort() {
    Collections.reverse(columns);
  }

  private List<ItemLegend> itemLegends;

  public BarData getBarDataValue() {
    return mBarDataValue;
  }

  public BarData getBarDataPercent() {
    return mBarDataPercent;
  }

  public void updateData() {
    itemLegends = new ArrayList<>();
    labels = new ArrayList<>();
    List<IBarDataSet> yValue = new ArrayList<>();
    List<IBarDataSet> yPercent = new ArrayList<>();
    List<Integer> colors = new ArrayList<>();

    columnPieceStylesMap = getMapStylePeice();
    if (columns == null || columns.size() == 0) {
      return;
    }

    GroupColumn groupColumn = columns.get(0);
    if (null == groupColumn) return;

    ArrayList<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
    if (null == dataColumnPieces || dataColumnPieces.isEmpty()) return;

    for (DataColumnPiece dataColumnPiece : dataColumnPieces) {
      try {
        Integer styleId = dataColumnPiece.getStyleId();
        if (null == styleId) {
          continue;
        }

        String color = columnPieceStylesMap.get(styleId).getColor();
        ItemLegend legend = new ItemLegend(
            columnPieceStylesMap.get(styleId).getTitle(),
            null == color ? Color.BLACK : Color.parseColor(color),
            "", "");
        legend.setObjectType(objectType);
        if (columnPieceStylesMap.get(styleId).getObjectId() != null)
          legend.setObjectId(columnPieceStylesMap.get(styleId).getObjectId().toString());
        itemLegends.add(legend);
        colors.add(null == color ? Color.BLACK : Color.parseColor(color));
      } catch (Exception e) {
        Logger.log(e);
      }
    }
    for (int i = 0; i < columns.size(); i++) {
      try {
        labels.add(columns.get(i).getLabel());
//      List<BarEntry> entries = new ArrayList<>();

//      entries.add(getBarEntryValue(i));
//      BarDataSet barDataSet = new BarDataSet(entries, "");
//      yValue.add(new BarData(getBarEntryValue(i)));
//      yPercent.add(getBarEntryPercent(i));
        getBarEntryValue(yValue, i);
        getBarEntryPercent(yPercent, i);
      } catch (Exception e) {
        Logger.log(e);
      }
    }
//    BarDataSet barDataSetValue = new BarDataSet(yValue, "");
//    BarDataSet barDataSetPercent = new BarDataSet(yPercent, "");
//
//    barDataSetValue.setColors(colors);
//    barDataSetValue.setHighLightAlpha(0);
//    barDataSetPercent.setColors(colors);
//    barDataSetPercent.setHighLightAlpha(0);

    mBarDataValue = new BarData(yValue);
    mBarDataPercent = new BarData(yPercent);
  }

  public HashMap<Integer, GroupColumnPieceStyle> getMapStylePeice() {
    if (null == columnPieceStyles || columnPieceStyles.isEmpty()) return new HashMap<>();

    HashMap<Integer, GroupColumnPieceStyle> mapStyle = new HashMap<>();
    for (GroupColumnPieceStyle s : columnPieceStyles) {
      mapStyle.put(s.getId(), s);
    }
    return mapStyle;
  }

  private void getBarEntryValue(List<IBarDataSet> barDataSets, int entryIndex) {

    GroupColumn groupColumn = columns.get(entryIndex);
    if (null == groupColumn) return;

    ArrayList<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
    if (null == dataColumnPieces || dataColumnPieces.isEmpty()) return;

    int size = dataColumnPieces.size();
    List<Integer> colors = new ArrayList<>();
    List<Float> value = new ArrayList<>();
    for (int j = 0; j < size; j++) {
      try {
        DataColumnPiece dataColumnPiece = dataColumnPieces.get(j);
        if (null == dataColumnPiece) continue;

        String color = columnPieceStylesMap.get(dataColumnPiece.getStyleId()).getColor();
        Float v = dataColumnPiece.getValue();
        if (v != null) {
          value.add(v);
          colors.add(null == color ? Color.BLACK : Color.parseColor(color));
        }
      } catch (Exception e) {
        Logger.log(e);
      }
    }
    List<BarEntry> barEntries = new ArrayList<>();
    barEntries.add(new BarEntry(entryIndex, Floats.toArray(value), groupColumn.getLabel()));
    BarDataSet barDataSet = new BarDataSet(barEntries, "");
    barDataSet.setColors(colors);
    barDataSets.add(barDataSet);
  }

  private void getBarEntryPercent(List<IBarDataSet> barDataSets, int entryIndex) {

    GroupColumn groupColumn = columns.get(entryIndex);
    if (null == groupColumn) return;

    ArrayList<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
    List<Float> value = new ArrayList<>();
    List<Integer> colors = new ArrayList<>();

    if (null != dataColumnPieces && !dataColumnPieces.isEmpty()) {
      int size = dataColumnPieces.size();
      for (int j = 0; j < size; j++) {
        try {
          DataColumnPiece dataColumnPiece = dataColumnPieces.get(j);
          if (null == dataColumnPiece) continue;

          String color = columnPieceStylesMap.get(dataColumnPiece.getStyleId()).getColor();
          Float v = dataColumnPiece.getDrawValue();
          if (v != null) {
            value.add(v);
            colors.add(null == color ? Color.BLACK : Color.parseColor(color));
          }
        } catch (Exception e) {
          Logger.log(e);
        }
      }
    } else {
      value.add(0f);
      colors.add(Color.BLACK);
    }

    List<BarEntry> barEntries = new ArrayList<>();
    barEntries.add(new BarEntry(entryIndex, Floats.toArray(value), columns.get(entryIndex).getLabel()));
    BarDataSet barDataSet = new BarDataSet(barEntries, "");
    barDataSet.setColors(colors);
    barDataSets.add(barDataSet);
  }

  public PopOverChartModel getGroupDetail(BarEntry entry) {
    BarDataSet barDatasetPercent = (BarDataSet) mBarDataPercent.getDataSetForEntry(entry);
//    int entryIndex = barDataSetValue.getEntryIndex(entry);
//    if (entryIndex == -1) {
//    int entryIndex = barDatasetPercent.getEntryIndex(entry);
    int entryIndex = mBarDataPercent.getIndexOfDataSet(barDatasetPercent);
//    }
    return getGroupDetail(entryIndex);
  }

  public PopOverChartModel getGroupDetail(int index) {
    PopOverChartModel popOverChartModel = new PopOverChartModel();
    List<ItemLegend> itemLegends = new ArrayList<>();

    GroupColumn groupColumn = columns.get(index);
    if (null == groupColumn) return null;

    ArrayList<DataColumnPiece> dataColumnPieces = groupColumn.getDataColumnPieces();
    if (null != dataColumnPieces && !dataColumnPieces.isEmpty()) {
      for (int i = 0; i < dataColumnPieces.size(); i++) {
        DataColumnPiece dataColumnPiece = dataColumnPieces.get(i);
        if (null == dataColumnPiece) continue;

        String percent;
        String value = null;

        if (dataColumnPiece.getValue() != null) {
          value = NumberUtils.formatValue(dataColumnPiece.getValue());
        }
        if (dataColumnPiece.getPercent() == null)
          percent = "";
        else
          percent = dataColumnPiece.getPercentString();

        Integer styleId = dataColumnPiece.getStyleId();
        if (null == styleId) {
          continue;
        }

        String color = columnPieceStylesMap.get(styleId).getColor();
        ItemLegend itemLegend = new ItemLegend(columnPieceStylesMap.get(styleId).getTitle(),
            null == color ? Color.BLACK : Color.parseColor(color),
            percent, value);
        itemLegend.setObjectType(objectType);
        Integer objectId = columnPieceStylesMap.get(styleId).getObjectId();
        if (objectId != null)
          itemLegend.setObjectId(objectId.toString());
        itemLegend.setLegendType(LegendType.BARCHART);
        itemLegends.add(itemLegend);
      }
    }
    popOverChartModel.setTitle(chartName + " (" + unit + ")");
    popOverChartModel.setColumnValue(columns.get(index).getLabel());
    popOverChartModel.setItemLegends(itemLegends);
    return popOverChartModel;
  }
}
