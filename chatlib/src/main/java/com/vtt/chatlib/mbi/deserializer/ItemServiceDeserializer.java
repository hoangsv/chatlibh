package com.vtt.chatlib.mbi.deserializer;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.vtt.chatlib.mbi.constant.ServiceType;
import com.vtt.chatlib.mbi.dto.BaseItemService;
import com.vtt.chatlib.mbi.dto.blockInfo.InfoBlock;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitData;
import com.vtt.chatlib.mbi.dto.lineChart.ItemLineChartServiceDTO;
import com.vtt.chatlib.utils.Logger;

import org.apache.commons.lang3.EnumUtils;

import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * Created by HoaPham on 4/25/17.
 */

public class ItemServiceDeserializer implements JsonDeserializer<BaseItemService> {

  @Override
  public BaseItemService deserialize(JsonElement json, Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
    JsonObject jObj = json.getAsJsonObject();
    BaseItemService baseItemService = new BaseItemService();
    String type = jObj.get("type").getAsString();
    if (!EnumUtils.isValidEnum(ServiceType.class, type)) {
      baseItemService.setType("UNKNOWN");
      return baseItemService;
    }
    baseItemService.setType(type);

    if (jObj.has("blockId") && !(jObj.get("blockId") instanceof JsonNull)) {
      baseItemService.setBlockId(jObj.get("blockId").getAsString());
    }

    if (jObj.has("id") && !(jObj.get("id") instanceof JsonNull))
      baseItemService.setId(jObj.get("id").getAsString());

    ServiceType typeItemDashboard = ServiceType.valueOf(type);
    JsonElement dataJson = jObj.get("data");
    switch (typeItemDashboard) {
//      case ITEM_DASHBOARD:
//        try {
//          baseItemService.setData(new Gson().fromJson(dataJson, ItemDashboard.class));
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
      case LINE_CHART:
        try {
          ItemLineChartServiceDTO dto = new Gson().fromJson(dataJson, ItemLineChartServiceDTO.class);
          dto.updateChartData();
//          if (dto.getFilterMetadata() != null)
//            dto.getFilterMetadata().updateFilterData(App.getInstance());
          baseItemService.setData(dto);
        } catch (Exception e) {
          Logger.log(e);
          baseItemService.setType(ServiceType.UNKNOWN.toString());
        }
        break;
//      case COLUMN_CHART:
//        try {
//          ItemColumnChartService itemColumnChartService = new Gson()
//              .fromJson(dataJson, ItemColumnChartService.class);
//          itemColumnChartService.updateBarData();
//          if (itemColumnChartService.getFilterMetadata() != null)
//            itemColumnChartService.getFilterMetadata().updateFilterData(App.getInstance());
//          baseItemService.setData(itemColumnChartService);
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
//      case CIRCLE_CHART:
//        try {
//          ItemPieChartService pieChartService = new Gson().fromJson(dataJson, ItemPieChartService.class);
//          if (pieChartService.getFilterMetadata() != null)
//            pieChartService.getFilterMetadata().updateFilterData(App.getInstance());
//          baseItemService.setData(pieChartService);
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
//      case VERTICAL_STACK_COLUMN_CHART:
//        try {
//          ItemVerticalStackColumnChartService itemVerticalStackColumnChartService = new Gson()
//              .fromJson(dataJson, ItemVerticalStackColumnChartService.class);
//          itemVerticalStackColumnChartService.getChartData().updateData();
//          if (itemVerticalStackColumnChartService.getFilterMetadata() != null)
//            itemVerticalStackColumnChartService.getFilterMetadata().updateFilterData(App.getInstance());
//          baseItemService.setData(itemVerticalStackColumnChartService);
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
//      case AREA_CHART:
//        try {
//          AreaChartData areaChartData = new Gson().fromJson(dataJson, AreaChartData.class);
//          areaChartData.updateChartData();
//          if (areaChartData.getFilterMetadata() != null)
//            areaChartData.getFilterMetadata().updateFilterData(App.getInstance());
//          baseItemService.setData(areaChartData);
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
//      case COMBINE_30_DAYS:
//      case COMBINE_CHART:
//        try {
//          ItemCombineChartService itemCombineChartService = new Gson().fromJson(dataJson, ItemCombineChartService.class);
//          if (itemCombineChartService == null)
//            break;
//          if (itemCombineChartService.getmGroupColumnChart() != null) {
//            if (itemCombineChartService.getmGroupColumnChart().getChartName() == null) {
//              itemCombineChartService.getmGroupColumnChart().setChartName(itemCombineChartService.getChartName());
//            }
//            itemCombineChartService.getmGroupColumnChart().updateBarData();
//          }
//          if (itemCombineChartService.getmItemLineChart() != null) {
//            if (itemCombineChartService.getmItemLineChart().getChartTitle() == null) {
//              itemCombineChartService.getmItemLineChart().setChartTitle(itemCombineChartService.getChartName());
//            }
//            itemCombineChartService.getmItemLineChart().updateChartData();
//          }
//          if (itemCombineChartService.getFilterMetadata() != null)
//            itemCombineChartService.getFilterMetadata().updateFilterData(App.getInstance());
//          baseItemService.setData(itemCombineChartService);
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
//      case COMBINE_LINE_CHART:
//        try {
//          ItemLineCombineChartService item = new Gson().fromJson(dataJson, ItemLineCombineChartService.class);
//          if (item != null) {
//            String[] serviceIds = null;
//            if (item.getFilterMetadata() != null) {
//              item.getFilterMetadata().updateFilterData(App.getInstance());
//              if (item.getFilterMetadata().getServiceId() != null)
//                serviceIds = item.getFilterMetadata().getServiceId().split(",");
//            }
//
////            item.updateChartData();
//            int numberLeft = 0;
//            if (item.getmLineLeft() != null) {
//              if (item.getmLineLeft().getChartTitle() == null) {
//                item.getmLineLeft().setChartTitle(item.getChartName());
//              }
//              item.getmLineLeft().setDates(item.getDates());
//              if (null != serviceIds && serviceIds.length > 0) {
//                if (item.getmLineLeft().getLines() != null)
//                  numberLeft = item.getmLineLeft().getLines().size();
//                item.getmLineLeft().updateChartData(Arrays.copyOfRange(serviceIds, 0, item.getmLineLeft().getLines().size()));
//              } else
//                item.getmLineLeft().updateChartData();
//            }
//            if (item.getmLineRight() != null) {
//              if (item.getmLineRight().getChartTitle() == null) {
//                item.getmLineRight().setChartTitle(item.getChartName());
//              }
//              item.getmLineRight().setDates(item.getDates());
//              if (null != serviceIds && serviceIds.length > 1) {
//                item.getmLineRight().updateChartData(Arrays.copyOfRange(serviceIds, numberLeft, serviceIds.length));
//              } else
//                item.getmLineRight().updateChartData();
//            }
//            item.updateChartData();
//            baseItemService.setData(item);
//          }
//        } catch (Exception e) {
//          e.printStackTrace();
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
      case HORIZONTAL_STACK_RANKING_UNIT:
        try {
          ItemHorizontalStackRankingUnitData rankingUnitData = new Gson().fromJson(dataJson, ItemHorizontalStackRankingUnitData.class);
          if (rankingUnitData.getChartData() != null)
            rankingUnitData.getChartData().updateData();
          baseItemService.setData(rankingUnitData);
        } catch (Exception e) {
          Logger.log(e);
          baseItemService.setType(ServiceType.UNKNOWN.toString());
        }
        break;
      case SERVICE_REPORT:
      case CLASS_REPORT:
      case RANKING_INFO:
        try {
          InfoBlock infoBlock = new Gson().fromJson(dataJson, InfoBlock.class);
//          if (infoBlock != null && infoBlock.getFilterMetadata() != null)
//            infoBlock.getFilterMetadata().updateFilterData(App.getInstance());
          baseItemService.setData(infoBlock);
        } catch (Exception e) {
          Logger.log(e);
          baseItemService.setType(ServiceType.UNKNOWN.toString());
        }
        break;
//      case TABLE_VIEW:
//        try {
//          Gson gson = new GsonBuilder()
//              .registerTypeAdapter(TableCell.class, new CellDeserializer())
//              .create();
//          baseItemService.setData(gson.fromJson(dataJson, TableRanking.class));
//        } catch (Exception e) {
//          baseItemService.setType(ServiceType.UNKNOWN.toString());
//        }
//        break;
      default:
        baseItemService.setType(ServiceType.UNKNOWN.toString());
        break;
    }
    return baseItemService;
  }
}
