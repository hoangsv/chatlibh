package com.vtt.chatlib.chatkit.models;

import com.github.mikephil.charting.data.ChartData;

/**
 * baseclass of the chart-listview items
 *
 * @author philipp
 */
public class ChartItem {

  protected static final int TYPE_BARCHART = 0;
  protected static final int TYPE_LINECHART = 1;
  protected static final int TYPE_PIECHART = 2;

  protected ChartData<?> mChartData;

  public ChartItem(ChartData<?> cd) {
    this.mChartData = cd;
  }
}
