package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vtt.chatlib.R;
import com.vtt.chatlib.chatkit.models.Button;

import java.util.List;

/**
 * Created by HaiKE on 10/9/17.
 */

public class ButtonListAdapter extends RecyclerView.Adapter<ButtonHolder> {
  protected List<Button> datas;
  private Context context;
  private MessagesListAdapter.OnMessageButtonClickListener onMessageButtonClickListener;

  public ButtonListAdapter(Context context, List<Button> datas, MessagesListAdapter.OnMessageButtonClickListener onMessageButtonClickListener) {
    super();
    this.context = context;
    this.datas = datas;
    this.onMessageButtonClickListener = onMessageButtonClickListener;
  }

  @Override
  public int getItemCount() {
    if (datas != null && datas.size() > 0)
      return datas.size();
    return 0;
  }

  @Override
  public ButtonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_button, parent, false);
    return new ButtonHolder(view, context);
  }

  @SuppressWarnings("deprecation")
  @Override
  public void onBindViewHolder(ButtonHolder holder, final int position) {
    if (holder instanceof ButtonHolder) {
      ButtonHolder itemHolder = (ButtonHolder) holder;
      itemHolder.bind(datas.get(position));
      itemHolder.btnSubmit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (onMessageButtonClickListener != null)
            onMessageButtonClickListener.onMessageClick(datas.get(position));
        }
      });
    }
  }
}
