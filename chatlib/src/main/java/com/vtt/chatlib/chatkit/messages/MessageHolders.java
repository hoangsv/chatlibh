package com.vtt.chatlib.chatkit.messages;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.vtt.chatlib.R;
import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.chatheads.ChatHeadUtils;
import com.vtt.chatlib.chatkit.commons.ImageLoader;
import com.vtt.chatlib.chatkit.commons.ViewHolder;
import com.vtt.chatlib.chatkit.models.IMessage;
import com.vtt.chatlib.chatkit.models.MessageContentType;
import com.vtt.chatlib.chatkit.utils.DateFormatter;
import com.vtt.chatlib.chatkit.utils.RoundedImageView;
import com.vtt.chatlib.mbi.dto.blockInfo.InfoBlock;
import com.vtt.chatlib.mbi.dto.blockInfo.MainBlock;
import com.vtt.chatlib.mbi.dto.common.AreaDetailsAdapter;
import com.vtt.chatlib.mbi.dto.common.LegendsAdapter;
import com.vtt.chatlib.mbi.dto.horizontalChart.BarChartUtils;
import com.vtt.chatlib.mbi.dto.horizontalChart.ItemHorizontalStackRankingUnitData;
import com.vtt.chatlib.mbi.dto.horizontalChart.ViewType;
import com.vtt.chatlib.mbi.dto.lineChart.ItemLineChartServiceDTO;
import com.vtt.chatlib.mbi.dto.lineChart.LineChartUtils;
import com.vtt.chatlib.utils.Logger;
import com.vtt.chatlib.utils.NumberUtils;
import com.vtt.chatlib.utils.ProportionView;
import com.vtt.chatlib.utils.RecyclerUtils;
import com.vtt.chatlib.utils.StringUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/*
 * Created by troy379 on 31.03.17.
 */
@SuppressWarnings("WeakerAccess")
public class MessageHolders {

  private static final short VIEW_TYPE_DATE_HEADER = 130;
  private static final short VIEW_TYPE_TEXT_MESSAGE = 131;
  private static final short VIEW_TYPE_IMAGE_MESSAGE = 132;
  private static final short VIEW_TYPE_ACTION_MESSAGE = 133;
  private static final short VIEW_TYPE_IMAGE_LINK_MESSAGE = 135;
  private static final short VIEW_TYPE_QUICK_REPLY_MESSAGE = 136;
  private static final short VIEW_TYPE_IMAGE_LOADING_MESSAGE = 141;
  private static final short VIEW_TYPE_BLOCK_INFO_MESSAGE = 142;
  private static final short VIEW_TYPE_LINE_CHART_MESSAGE = 143;
  private static final short VIEW_TYPE_HORIZONTAL_RANKING_CHART_MESSAGE = 144;
  private static final short VIEW_TYPE_AVAILABLE_FEATURES_MESSAGE = 145;
  private static final short VIEW_TYPE_CONCLUSION_MESSAGE = 146;
  private static final short VIEW_TYPE_RANKING_INFO_MESSAGE = 147;

  private Class<? extends ViewHolder<Date>> dateHeaderHolder;
  private int dateHeaderLayout;

  private HolderConfig<IMessage> incomingTextConfig;
  private HolderConfig<IMessage> outcomingTextConfig;
  private HolderConfig<MessageContentType.Image> incomingImageConfig;
  private HolderConfig<MessageContentType.Image> outcomingImageConfig;
  private HolderConfig<MessageContentType.Link> incomingImageLinkConfig;
  private HolderConfig<MessageContentType.Link> outcomingImageLinkConfig;
  private HolderConfig<MessageContentType.ActionList> incomingActionConfig;
  private HolderConfig<MessageContentType.ActionList> outcomingActionConfig;
  private HolderConfig<MessageContentType.QuickReply> incomingQuickReplyConfig;
  private HolderConfig<MessageContentType.QuickReply> outcomingQuickReplyConfig;

  private HolderConfig<MessageContentType.ImageLoading> incomingImageLoadingConfig;
  private HolderConfig<MessageContentType.BlockInfo> incomingBlockInfoConfig;
  private HolderConfig<MessageContentType.LineChart> incomingLineChartConfig;
  private HolderConfig<MessageContentType.HorizontalRankChart> incomingHorizontalRankChartConfig;
  private HolderConfig<MessageContentType.AvailableFeatureType> incomingAvailableFeatureConfig;
  private HolderConfig<MessageContentType.ConclusionType> incomingConclusionMessageConfig;
  private HolderConfig<MessageContentType.RankingInfoType> incomingRankingInfoMessageConfig;

  private List<ContentTypeConfig> customContentTypes = new ArrayList<>();
  private ContentChecker contentChecker;
  private static Context context;

  private static MessagesListAdapter.OnMessageActionClickListener onMessageActionClickListener;
  private static AvailableFeatureAdapter.OnItemClick onItemAvailableFeatureClick;
  private static OnItemViewClick onItemViewClick;

  public void setOnMessageActionClickListener(MessagesListAdapter.OnMessageActionClickListener onMessageActionClickListener) {
    this.onMessageActionClickListener = onMessageActionClickListener;
  }

  public void setOnItemAvailableFeatureClick(AvailableFeatureAdapter.OnItemClick onItemAvailableFeatureClick) {
    this.onItemAvailableFeatureClick = onItemAvailableFeatureClick;
  }

  public void setOnItemViewConclusionClick(OnItemViewClick onItemViewClick) {
    this.onItemViewClick = onItemViewClick;
  }

  private static MessagesListAdapter.OnMessageButtonClickListener onMessageButtonClickListener;

  public void setOnMessageButtonClickListener(MessagesListAdapter.OnMessageButtonClickListener onMessageActionClickListener) {
    this.onMessageButtonClickListener = onMessageActionClickListener;
  }

  public MessageHolders(Context context) {
    this.context = context;
    this.dateHeaderHolder = DefaultDateHeaderViewHolder.class;
    this.dateHeaderLayout = R.layout.item_date_header;

    this.incomingTextConfig = new HolderConfig<>(DefaultIncomingTextMessageViewHolder.class, R.layout.item_incoming_text_message);
    this.outcomingTextConfig = new HolderConfig<>(DefaultOutcomingTextMessageViewHolder.class, R.layout.item_outcoming_text_message);
    this.incomingImageConfig = new HolderConfig<>(DefaultIncomingImageMessageViewHolder.class, R.layout.item_incoming_image_message);
    this.outcomingImageConfig = new HolderConfig<>(DefaultOutcomingImageMessageViewHolder.class, R.layout.item_outcoming_image_message);
    this.incomingImageLinkConfig = new HolderConfig<>(DefaultIncomingImageLinkMessageViewHolder.class, R.layout.item_incoming_image_link_message);
    this.outcomingImageLinkConfig = new HolderConfig<>(DefaultOutcomingImageLinkMessageViewHolder.class, R.layout.item_outcoming_image_link_message);

    this.incomingActionConfig = new HolderConfig<>(DefaultIncomingActionMessageViewHolder.class, R.layout.item_incoming_action_message);
    this.outcomingActionConfig = new HolderConfig<>(DefaultOutcomingActionMessageViewHolder.class, R.layout.item_incoming_action_message);

    this.incomingQuickReplyConfig = new HolderConfig<>(DefaultIncomingQuickReplyMessageViewHolder.class, R.layout.item_incoming_action_message);
    this.outcomingQuickReplyConfig = new HolderConfig<>(DefaultOutcomingQuickReplyMessageViewHolder.class, R.layout.item_incoming_action_message);

    this.incomingImageLoadingConfig = new HolderConfig<>(DefaultIncomingImageLoadingViewHolder.class, R.layout.item_incoming_image_message);
    this.incomingBlockInfoConfig = new HolderConfig<>(DefaultIncomingBlockInfoViewHolder.class, R.layout.item_incoming_block_info_message);
    this.incomingLineChartConfig = new HolderConfig<>(DefaultIncomingLineChartViewHolder.class, R.layout.item_incoming_service_chart);
    this.incomingHorizontalRankChartConfig = new HolderConfig<>(DefaultIncomingHorizontalRankChartViewHolder.class, R.layout.item_incoming_service_chart);
    this.incomingAvailableFeatureConfig = new HolderConfig<>(DefaultIncomingAvailableFeaturesViewHolder.class, R.layout.item_incoming_feature_message);
    this.incomingConclusionMessageConfig = new HolderConfig<>(DefaultIncomingConclusionMessageViewHolder.class, R.layout.item_incoming_conclusion_message);
    this.incomingRankingInfoMessageConfig = new HolderConfig<>(DefaultIncomingRankingInfoViewHolder.class, R.layout.item_incoming_ranking_info_message);
  }

  /**
   * Sets both of custom view holder class and layout resource for incoming text message.
   *
   * @param holder holder class.
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingTextConfig(
      @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
      @LayoutRes int layout) {
    this.incomingTextConfig.holder = holder;
    this.incomingTextConfig.layout = layout;
    return this;
  }

  /**
   * Sets custom view holder class for incoming text message.
   *
   * @param holder holder class.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingTextHolder(
      @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
    this.incomingTextConfig.holder = holder;
    return this;
  }

  /**
   * Sets custom layout resource for incoming text message.
   *
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingTextLayout(@LayoutRes int layout) {
    this.incomingTextConfig.layout = layout;
    return this;
  }

  /**
   * Sets both of custom view holder class and layout resource for outcoming text message.
   *
   * @param holder holder class.
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingTextConfig(
      @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder,
      @LayoutRes int layout) {
    this.outcomingTextConfig.holder = holder;
    this.outcomingTextConfig.layout = layout;
    return this;
  }

  /**
   * Sets custom view holder class for outcoming text message.
   *
   * @param holder holder class.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingTextHolder(
      @NonNull Class<? extends BaseMessageViewHolder<? extends IMessage>> holder) {
    this.outcomingTextConfig.holder = holder;
    return this;
  }

  /**
   * Sets custom layout resource for outcoming text message.
   *
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingTextLayout(@LayoutRes int layout) {
    this.outcomingTextConfig.layout = layout;
    return this;
  }

  /**
   * Sets both of custom view holder class and layout resource for incoming image message.
   *
   * @param holder holder class.
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingImageConfig(
      @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
      @LayoutRes int layout) {
    this.incomingImageConfig.holder = holder;
    this.incomingImageConfig.layout = layout;
    return this;
  }

  /**
   * Sets custom view holder class for incoming image message.
   *
   * @param holder holder class.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingImageHolder(
      @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
    this.incomingImageConfig.holder = holder;
    return this;
  }

  /**
   * Sets custom layout resource for incoming image message.
   *
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setIncomingImageLayout(@LayoutRes int layout) {
    this.incomingImageConfig.layout = layout;
    return this;
  }

  /**
   * Sets both of custom view holder class and layout resource for outcoming image message.
   *
   * @param holder holder class.
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingImageConfig(
      @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder,
      @LayoutRes int layout) {
    this.outcomingImageConfig.holder = holder;
    this.outcomingImageConfig.layout = layout;
    return this;
  }

  /**
   * Sets custom view holder class for outcoming image message.
   *
   * @param holder holder class.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingImageHolder(
      @NonNull Class<? extends BaseMessageViewHolder<? extends MessageContentType.Image>> holder) {
    this.outcomingImageConfig.holder = holder;
    return this;
  }

  /**
   * Sets custom layout resource for outcoming image message.
   *
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setOutcomingImageLayout(@LayoutRes int layout) {
    this.outcomingImageConfig.layout = layout;
    return this;
  }

  /**
   * Sets both of custom view holder class and layout resource for date header.
   *
   * @param holder holder class.
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setDateHeaderConfig(
      @NonNull Class<? extends ViewHolder<Date>> holder,
      @LayoutRes int layout) {
    this.dateHeaderHolder = holder;
    this.dateHeaderLayout = layout;
    return this;
  }

  /**
   * Sets custom view holder class for date header.
   *
   * @param holder holder class.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setDateHeaderHolder(@NonNull Class<? extends ViewHolder<Date>> holder) {
    this.dateHeaderHolder = holder;
    return this;
  }

  /**
   * Sets custom layout reource for date header.
   *
   * @param layout layout resource.
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public MessageHolders setDateHeaderLayout(@LayoutRes int layout) {
    this.dateHeaderLayout = layout;
    return this;
  }

  /**
   * Registers custom content type (e.g. multimedia, events etc.)
   *
   * @param type            unique id for content type
   * @param holder          holder class for incoming and outcoming messages
   * @param incomingLayout  layout resource for incoming message
   * @param outcomingLayout layout resource for outcoming message
   * @param contentChecker  {@link ContentChecker} for registered type
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public <TYPE extends MessageContentType>
  MessageHolders registerContentType(
      byte type, @NonNull Class<? extends BaseMessageViewHolder<TYPE>> holder,
      @LayoutRes int incomingLayout,
      @LayoutRes int outcomingLayout,
      @NonNull ContentChecker contentChecker) {

    return registerContentType(type,
        holder, incomingLayout,
        holder, outcomingLayout,
        contentChecker);
  }

  /**
   * Registers custom content type (e.g. multimedia, events etc.)
   *
   * @param type            unique id for content type
   * @param incomingHolder  holder class for incoming message
   * @param outcomingHolder holder class for outcoming message
   * @param incomingLayout  layout resource for incoming message
   * @param outcomingLayout layout resource for outcoming message
   * @param contentChecker  {@link ContentChecker} for registered type
   * @return {@link MessageHolders} for subsequent configuration.
   */
  public <TYPE extends MessageContentType>
  MessageHolders registerContentType(
      byte type,
      @NonNull Class<? extends BaseMessageViewHolder<TYPE>> incomingHolder, @LayoutRes int incomingLayout,
      @NonNull Class<? extends BaseMessageViewHolder<TYPE>> outcomingHolder, @LayoutRes int outcomingLayout,
      @NonNull ContentChecker contentChecker) {

    if (type == 0)
      throw new IllegalArgumentException("content type must be greater or less than '0'!");

    customContentTypes.add(
        new ContentTypeConfig<>(type,
            new HolderConfig<>(incomingHolder, incomingLayout),
            new HolderConfig<>(outcomingHolder, outcomingLayout)));
    this.contentChecker = contentChecker;
    return this;
  }

    /*
    * INTERFACES
    * */

  /**
   * The interface, which contains logic for checking the availability of content.
   */
  public interface ContentChecker<MESSAGE extends IMessage> {

    /**
     * Checks the availability of content.
     *
     * @param message current message in list.
     * @param type    content type, for which content availability is determined.
     * @return weather the message has content for the current message.
     */
    boolean hasContentFor(MESSAGE message, byte type);
  }

    /*
    * PRIVATE METHODS
    * */

  protected ViewHolder getHolder(ViewGroup parent, int viewType, MessagesListStyle messagesListStyle) {
    switch (viewType) {
      case VIEW_TYPE_DATE_HEADER:
        return getHolder(parent, dateHeaderLayout, dateHeaderHolder, messagesListStyle);
      case VIEW_TYPE_TEXT_MESSAGE:
        return getHolder(parent, incomingTextConfig, messagesListStyle);
      case -VIEW_TYPE_TEXT_MESSAGE:
        return getHolder(parent, outcomingTextConfig, messagesListStyle);
      case VIEW_TYPE_IMAGE_MESSAGE:
        return getHolder(parent, incomingImageConfig, messagesListStyle);
      case -VIEW_TYPE_IMAGE_MESSAGE:
        return getHolder(parent, outcomingImageConfig, messagesListStyle);
      case VIEW_TYPE_IMAGE_LINK_MESSAGE:
        return getHolder(parent, incomingImageLinkConfig, messagesListStyle);
      case -VIEW_TYPE_IMAGE_LINK_MESSAGE:
        return getHolder(parent, outcomingImageLinkConfig, messagesListStyle);
      case VIEW_TYPE_ACTION_MESSAGE:
        return getHolder(parent, incomingActionConfig, messagesListStyle);
      case -VIEW_TYPE_ACTION_MESSAGE:
        return getHolder(parent, outcomingActionConfig, messagesListStyle);
      case VIEW_TYPE_QUICK_REPLY_MESSAGE:
        return getHolder(parent, incomingQuickReplyConfig, messagesListStyle);
      case -VIEW_TYPE_QUICK_REPLY_MESSAGE:
        return getHolder(parent, outcomingQuickReplyConfig, messagesListStyle);
      case VIEW_TYPE_IMAGE_LOADING_MESSAGE:
        return getHolder(parent, incomingImageLoadingConfig, messagesListStyle);
      case VIEW_TYPE_BLOCK_INFO_MESSAGE:
        return getHolder(parent, incomingBlockInfoConfig, messagesListStyle);
      case VIEW_TYPE_LINE_CHART_MESSAGE:
        return getHolder(parent, incomingLineChartConfig, messagesListStyle);
      case VIEW_TYPE_HORIZONTAL_RANKING_CHART_MESSAGE:
        return getHolder(parent, incomingHorizontalRankChartConfig, messagesListStyle);
      case VIEW_TYPE_AVAILABLE_FEATURES_MESSAGE:
        return getHolder(parent, incomingAvailableFeatureConfig, messagesListStyle);
      case VIEW_TYPE_CONCLUSION_MESSAGE:
        return getHolder(parent, incomingConclusionMessageConfig, messagesListStyle);
      case VIEW_TYPE_RANKING_INFO_MESSAGE:
        return getHolder(parent, incomingRankingInfoMessageConfig, messagesListStyle);
      default:
        for (ContentTypeConfig typeConfig : customContentTypes) {
          if (Math.abs(typeConfig.type) == Math.abs(viewType)) {
            if (viewType > 0)
              return getHolder(parent, typeConfig.incomingConfig, messagesListStyle);
            else
              return getHolder(parent, typeConfig.outcomingConfig, messagesListStyle);
          }
        }
    }
    throw new IllegalStateException("Wrong message view type. Please, report this issue on GitHub with full stacktrace in description.");
  }

  @SuppressWarnings("unchecked")
  protected void bind(final ViewHolder holder, final Object item, boolean isSelected,
                      final ImageLoader imageLoader,
                      final View.OnClickListener onMessageClickListener,
                      final View.OnLongClickListener onMessageLongClickListener,
                      final DateFormatter.Formatter dateHeadersFormatter,
                      final SparseArray<MessagesListAdapter.OnMessageViewClickListener> clickListenersArray) {

    if (item instanceof IMessage) {
      ((BaseMessageViewHolder) holder).isSelected = isSelected;
      ((BaseMessageViewHolder) holder).imageLoader = imageLoader;
//      holder.itemView.setOnLongClickListener(onMessageLongClickListener);
      holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          if (onMessageLongClickListener != null)
            onMessageLongClickListener.onLongClick(v);
          return true;
        }
      });

      holder.itemView.setOnClickListener(onMessageClickListener);

      for (int i = 0; i < clickListenersArray.size(); i++) {
        final int key = clickListenersArray.keyAt(i);
        final View view = holder.itemView.findViewById(key);
        if (view != null) {
          view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              clickListenersArray.get(key).onMessageViewClick(view, (IMessage) item);
            }
          });
        }
      }
    } else if (item instanceof Date) {
      ((DefaultDateHeaderViewHolder) holder).dateHeadersFormatter = dateHeadersFormatter;
    }

    holder.onBind(item);
  }


  protected int getViewType(Object item, String senderId) {
    boolean isOutcoming = false;
    int viewType;

    if (item instanceof IMessage) {
      IMessage message = (IMessage) item;
      isOutcoming = message.getUser().getId().contentEquals(senderId);
      viewType = getContentViewType(message);

    } else viewType = VIEW_TYPE_DATE_HEADER;

    return isOutcoming ? viewType * -1 : viewType;
  }

  private ViewHolder getHolder(ViewGroup parent, HolderConfig holderConfig, MessagesListStyle style) {
    return getHolder(parent, holderConfig.layout, holderConfig.holder, style);
  }

  private <HOLDER extends ViewHolder>
  ViewHolder getHolder(ViewGroup parent, @LayoutRes int layout, Class<HOLDER> holderClass, MessagesListStyle style) {

    View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
    try {
      Constructor<HOLDER> constructor = holderClass.getDeclaredConstructor(View.class);
      constructor.setAccessible(true);
      HOLDER holder = constructor.newInstance(v);
      if (holder instanceof DefaultMessageViewHolder && style != null) {
        ((DefaultMessageViewHolder) holder).applyStyle(style);
      }
      return holder;
    } catch (Exception e) {
      Logger.log(e);
      return null;
    }
  }

  @SuppressWarnings("unchecked")
  private short getContentViewType(IMessage message) {
    if (message instanceof MessageContentType.Image
        && ((MessageContentType.Image) message).getImageUrl() != null) {
      return VIEW_TYPE_IMAGE_MESSAGE;
    }

    if (message instanceof MessageContentType.Link
        && ((MessageContentType.Link) message).getLinkImage() != null) {
      return VIEW_TYPE_IMAGE_LINK_MESSAGE;
    }

    if (message instanceof MessageContentType.QuickReply
        && ((MessageContentType.QuickReply) message).getActionList() != null
        && ((MessageContentType.QuickReply) message).getActionList().size() > 0) {
      return VIEW_TYPE_QUICK_REPLY_MESSAGE;
    }

    if (message instanceof MessageContentType.ActionList
        && ((MessageContentType.ActionList) message).getButtonList() != null
        && ((MessageContentType.ActionList) message).getButtonList().size() > 0) {
      return VIEW_TYPE_ACTION_MESSAGE;
    }

    // other default types will be here

    if (message instanceof MessageContentType) {
      for (int i = 0; i < customContentTypes.size(); i++) {
        ContentTypeConfig config = customContentTypes.get(i);
        if (contentChecker == null) {
          throw new IllegalArgumentException("ContentChecker cannot be null when using custom content types!");
        }
        boolean hasContent = contentChecker.hasContentFor(message, config.type);
        if (hasContent) return config.type;
      }
    }

    if (message instanceof MessageContentType.ImageLoading
        && ((MessageContentType.ImageLoading) message).getImageResource() != null) {
      return VIEW_TYPE_IMAGE_LOADING_MESSAGE;
    }

    if (message instanceof MessageContentType.BlockInfo
        && ((MessageContentType.BlockInfo) message).getInfoBlock() != null) {
      return VIEW_TYPE_BLOCK_INFO_MESSAGE;
    }

    if (message instanceof MessageContentType.LineChart
        && ((MessageContentType.LineChart) message).getItemLineChart() != null) {
      return VIEW_TYPE_LINE_CHART_MESSAGE;
    }

    if (message instanceof MessageContentType.HorizontalRankChart
        && ((MessageContentType.HorizontalRankChart) message).getItemHorizontalStackRankingChart() != null) {
      return VIEW_TYPE_HORIZONTAL_RANKING_CHART_MESSAGE;
    }

    if (message instanceof MessageContentType.AvailableFeatureType && ((MessageContentType.AvailableFeatureType) message).getAvailableFeatures() != null) {
      return VIEW_TYPE_AVAILABLE_FEATURES_MESSAGE;
    }

    if (message instanceof MessageContentType.ConclusionType && ((MessageContentType.ConclusionType) message).getConclusionMessage() != null) {
      return VIEW_TYPE_CONCLUSION_MESSAGE;
    }

    if (message instanceof MessageContentType.RankingInfoType && ((MessageContentType.RankingInfoType) message).getRankingInfo() != null) {
      return VIEW_TYPE_RANKING_INFO_MESSAGE;
    }

    return VIEW_TYPE_TEXT_MESSAGE;
  }

    /*
    * HOLDERS
    * */

  /**
   * The base class for view holders for incoming and outcoming message.
   * You can extend it to create your own holder in conjuction with custom layout or even using default layout.
   */
  public static abstract class BaseMessageViewHolder<MESSAGE extends IMessage> extends ViewHolder<MESSAGE> {

    boolean isSelected;

    /**
     * Callback for implementing images loading in message list
     */
    protected ImageLoader imageLoader;

    public BaseMessageViewHolder(View itemView) {
      super(itemView);
    }

    /**
     * Returns whether is item selected
     *
     * @return weather is item selected.
     */
    public boolean isSelected() {
      return isSelected;
    }

    /**
     * Returns weather is selection mode enabled
     *
     * @return weather is selection mode enabled.
     */
    public boolean isSelectionModeEnabled() {
      return MessagesListAdapter.isSelectionModeEnabled;
    }

    /**
     * Getter for {@link #imageLoader}
     *
     * @return image loader interface.
     */
    public ImageLoader getImageLoader() {
      return imageLoader;
    }

    protected void configureLinksBehavior(final TextView text) {
      text.setLinksClickable(false);
      text.setMovementMethod(new LinkMovementMethod() {
        @Override
        public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
          boolean result = false;
          if (!MessagesListAdapter.isSelectionModeEnabled) {
            result = super.onTouchEvent(widget, buffer, event);
          }
          itemView.onTouchEvent(event);
          return result;
        }
      });
    }

  }

  /**
   * Default view holder implementation for incoming text message
   */
  public static class IncomingTextMessageViewHolder<MESSAGE extends IMessage>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    protected ViewGroup bubble;
    protected TextView text;

    public IncomingTextMessageViewHolder(View itemView) {
      super(itemView);
      bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
      text = (TextView) itemView.findViewById(R.id.messageText);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (bubble != null) {
        bubble.setSelected(isSelected());
      }

      if (text != null) {
        text.setText(message.getText());
      }
    }

    @Override
    public void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (bubble != null) {
//        bubble.setPadding(style.getIncomingDefaultBubblePaddingLeft(),
//            style.getIncomingDefaultBubblePaddingTop(),
//            style.getIncomingDefaultBubblePaddingRight(),
//            style.getIncomingDefaultBubblePaddingBottom());
        ViewCompat.setBackground(bubble, style.getIncomingBubbleDrawable());
      }

      if (text != null) {
        text.setTextColor(style.getIncomingTextColor());
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingTextSize());
        text.setTypeface(text.getTypeface(), style.getIncomingTextStyle());
        text.setAutoLinkMask(style.getTextAutoLinkMask());
        text.setLinkTextColor(style.getIncomingTextLinkColor());
        configureLinksBehavior(text);
      }
    }
  }

  /**
   * Default view holder implementation for outcoming text message
   */
  public static class OutcomingTextMessageViewHolder<MESSAGE extends IMessage>
      extends BaseOutcomingMessageViewHolder<MESSAGE> {

    protected ViewGroup bubble;
    protected TextView text;

    public OutcomingTextMessageViewHolder(View itemView) {
      super(itemView);
      bubble = (ViewGroup) itemView.findViewById(R.id.bubble);
      text = (TextView) itemView.findViewById(R.id.messageText);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (bubble != null) {
        bubble.setSelected(isSelected());
      }

      if (text != null) {
        text.setText(message.getText());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (bubble != null) {
//        bubble.setPadding(style.getOutcomingDefaultBubblePaddingLeft(),
//            style.getOutcomingDefaultBubblePaddingTop(),
//            style.getOutcomingDefaultBubblePaddingRight(),
//            style.getOutcomingDefaultBubblePaddingBottom());
        ViewCompat.setBackground(bubble, style.getOutcomingBubbleDrawable());
      }

      if (text != null) {
        text.setTextColor(style.getOutcomingTextColor());
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getOutcomingTextSize());
        text.setTypeface(text.getTypeface(), style.getOutcomingTextStyle());
        text.setAutoLinkMask(style.getTextAutoLinkMask());
        text.setLinkTextColor(style.getOutcomingTextLinkColor());
        configureLinksBehavior(text);
      }
    }
  }

  /**
   * Default view holder implementation for incoming image message
   */
  public static class IncomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    protected ImageView image;
    protected View imageOverlay;

    public IncomingImageMessageViewHolder(View itemView) {
      super(itemView);
      image = (ImageView) itemView.findViewById(R.id.image);
      imageOverlay = itemView.findViewById(R.id.imageOverlay);
      image.setVisibility(View.VISIBLE);
      itemView.findViewById(R.id.image_loading).setVisibility(View.GONE);

      if (image != null && image instanceof RoundedImageView) {
        ((RoundedImageView) image).setCorners(
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            0
        );
      }
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (image != null) {
        Glide.with(itemView.getContext()).load(Integer.parseInt(message.getImageUrl())).into(image);
      }

      if (imageOverlay != null) {
        imageOverlay.setSelected(isSelected());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getIncomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
      }

      if (imageOverlay != null) {
        ViewCompat.setBackground(imageOverlay, style.getIncomingImageOverlayDrawable());
      }
    }
  }

  /**
   * Default view holder implementation for outcoming image message
   */
  public static class OutcomingImageMessageViewHolder<MESSAGE extends MessageContentType.Image>
      extends BaseOutcomingMessageViewHolder<MESSAGE> {

    protected ImageView image;
    protected View imageOverlay;

    public OutcomingImageMessageViewHolder(View itemView) {
      super(itemView);
      image = (ImageView) itemView.findViewById(R.id.image_message);
      imageOverlay = itemView.findViewById(R.id.imageOverlay);
      image.setVisibility(View.VISIBLE);

      if (image != null && image instanceof RoundedImageView) {
        ((RoundedImageView) image).setCorners(
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            0,
            R.dimen.message_bubble_corners_radius
        );
      }
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (image != null) {
//        Glide.with(itemView.getContext()).load(Integer.parseInt(message.getImageUrl())).into(image);
        image.setImageResource(Integer.parseInt(message.getImageUrl()));
      }

      time.setVisibility(View.GONE);
      if (imageOverlay != null) {
        imageOverlay.setSelected(isSelected());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getOutcomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getOutcomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getOutcomingImageTimeTextStyle());
      }

      if (imageOverlay != null) {
        ViewCompat.setBackground(imageOverlay, style.getOutcomingImageOverlayDrawable());
      }
    }
  }

  public static class IncomingImageLoadingViewHolder<MESSAGE extends MessageContentType.ImageLoading>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    protected ImageView imageLoading;
    protected View imageOverlay;

    public IncomingImageLoadingViewHolder(View itemView) {
      super(itemView);
      imageLoading = (GifImageView) itemView.findViewById(R.id.image_loading);
      imageLoading.setVisibility(View.VISIBLE);
      itemView.findViewById(R.id.image).setVisibility(View.GONE);
      imageOverlay = itemView.findViewById(R.id.imageOverlay);

    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (imageLoading != null && context != null) {
        try {
          GifDrawable gifFromResource = new GifDrawable(context.getResources(), R.raw.ic_chat_loading);
          imageLoading.setImageDrawable(gifFromResource);
        } catch (IOException e) {
          Logger.log(e);
        }

      }

      if (time != null) time.setVisibility(View.GONE);

      if (imageOverlay != null) {
        imageOverlay.setSelected(isSelected());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getIncomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
      }

      if (imageOverlay != null) {
        ViewCompat.setBackground(imageOverlay, style.getIncomingImageOverlayDrawable());
      }
    }
  }

  public static class IncomingBlockInfoViewHolder<MESSAGE extends MessageContentType.BlockInfo>
      extends BaseIncomingMessageViewHolder<MESSAGE> {
    public TextView targetNameTv;
    public TextView labelInfoTv;
    public TextView valueTv;
    public TextView unitTv;
    public ProportionView proportionV;
    public RecyclerView subBlockInfoRv;
    public RelativeLayout layoutContainer;

    public IncomingBlockInfoViewHolder(View itemView) {
      super(itemView);
      targetNameTv = (TextView) itemView.findViewById(R.id.target_name_tv);
      labelInfoTv = (TextView) itemView.findViewById(R.id.label_info_tv);
      valueTv = (TextView) itemView.findViewById(R.id.value_tv);
      unitTv = (TextView) itemView.findViewById(R.id.unit_tv);
      proportionV = (ProportionView) itemView.findViewById(R.id.perform_value_pv);
      subBlockInfoRv = (RecyclerView) itemView.findViewById(R.id.sub_block_rv);
      layoutContainer = (RelativeLayout) itemView.findViewById(R.id.layout_container);
    }

    @Override
    public void onBind(final MESSAGE message) {
      super.onBind(message);

      final InfoBlock data = message.getInfoBlock();
      if (data == null) {
        itemView.setVisibility(View.GONE);
        return;
      }

      String targetName = data.getReportName();
      if (targetName == null || targetName.isEmpty())
        targetNameTv.setVisibility(View.GONE);
      else
        targetNameTv.setText(targetName);

//      if (data.getFilterMetadata() != null) {
//        String label = data.getFilterMetadata().getContentSearch(holder.itemView.getContext());
//        labelInfoTv.setText(StringUtils.checkNullString(label));
//      } else {
//        labelInfoTv.setText("");
//      }
      MainBlock block = data.getMainBlock();
      if (block != null) {
        proportionV.setVisibility(View.VISIBLE);
        unitTv.setText(StringUtils.checkNullString(data.getUnit()));
        valueTv.setText(StringUtils.checkNullString(block.getPerformValue()));
        if (block.getPlanValue() != null || block.getPerformPercent() != null) {
          String proportionText = "";
          if (block.getPlanValue() == null || "".equals(block.getPlanValue())) {
            proportionText += StringUtils.checkNullString(
                NumberUtils.formatPercentWithCheckEmpty(itemView.getContext(), block.getPerformPercent()));
          } else {
            proportionText += block.getPlanValue();
            if (block.getPerformPercent() != null) {
              proportionText += " (" + NumberUtils.formatPercentWithCheckEmpty(itemView.getContext(), block.getPerformPercent()) + ")";
            }
          }
          if (!StringUtils.isEmptyNA(proportionText)) {
            proportionV.setText(proportionText);
            proportionV.setPercentComplete(block.getPerformPercent());
            proportionV.setColor(Color.parseColor(block.getPerformColor()));
          } else {
            proportionV.setVisibility(View.GONE);
          }
        } else {
          proportionV.setVisibility(View.GONE);
        }
      } else {
        unitTv.setText("");
        valueTv.setText("");
        proportionV.setVisibility(View.GONE);
      }
      RecyclerUtils.setupGridRecyclerView(itemView.getContext(), subBlockInfoRv, 2);
      if (data.getOtherInfo() != null) {
        subBlockInfoRv.setVisibility(View.VISIBLE);
//      holder.subBlockInfoRv.setAdapter(new SubInfoBlockAdapter(data.getOtherInfo()));
        subBlockInfoRv.setAdapter(new AreaDetailsAdapter(data.getOtherInfo()));
      } else {
        subBlockInfoRv.setVisibility(View.GONE);
      }

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (onItemViewClick != null) onItemViewClick.onItemChartClick(message.getPayload());
        }
      });

//      itemView.post(new Runnable() {
//        @Override
//        public void run() {
//          int height = itemView.getHeight();
//          int weight = itemView.getWidth();
//          int targetHeight = weight / 16 * 9;
//          float destini = (float) targetHeight / (float) height;
//          itemView.setScaleY(destini);
//        }
//      });

      if (time != null) time.setVisibility(View.GONE);

    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  public static class IncomingRankingInfoViewHolder<MESSAGE extends MessageContentType.RankingInfoType>
      extends BaseIncomingMessageViewHolder<MESSAGE> {
    public TextView valueTv;
    public TextView unitTv;
    public RecyclerView subBlockInfoRv;
    public RelativeLayout layoutContainer;
    View infoBlockMainXepHang;
    View infoBlockMainXepHang1;
    TextView txtInfoBlockMainRank1;
    TextView txtUnit1;
    TextView txtYesterdayRank1;
    TextView txtInfoBlockMainRank2;
    TextView txtUnit2;
    TextView txtYesterdayRank2;
    ImageView imgDelta1;
    ImageView imgDelta2;
    TextView txtLevel2;
    TextView txtLevel1;
    TextView txtSubtitle;

    public IncomingRankingInfoViewHolder(View itemView) {
      super(itemView);
      valueTv = (TextView) itemView.findViewById(R.id.value_tv_ranking);
      unitTv = (TextView) itemView.findViewById(R.id.unit_tv_ranking);
      subBlockInfoRv = (RecyclerView) itemView.findViewById(R.id.sub_block_rv_ranking);
      layoutContainer = (RelativeLayout) itemView.findViewById(R.id.layout_container_ranking);
      infoBlockMainXepHang = itemView.findViewById(R.id.info_block_main_xep_hang);
      infoBlockMainXepHang1 = itemView.findViewById(R.id.main_block_xep_hang_1);
      txtInfoBlockMainRank1 = (TextView) itemView.findViewById(R.id.txt_rank_1);
      txtUnit1 = (TextView) itemView.findViewById(R.id.txt_unit_1);
      txtYesterdayRank1 = (TextView) itemView.findViewById(R.id.txt_yesterday_ranking_1);
      txtInfoBlockMainRank2 = (TextView) itemView.findViewById(R.id.txt_rank_2);
      txtUnit2 = (TextView) itemView.findViewById(R.id.txt_unit_2);
      txtYesterdayRank2 = (TextView) itemView.findViewById(R.id.txt_yesterday_ranking_2);
      imgDelta1 = (ImageView) itemView.findViewById(R.id.img_delta_1);
      imgDelta2 = (ImageView) itemView.findViewById(R.id.img_delta_2);
      txtLevel2 = (TextView) itemView.findViewById(R.id.txt_level_2);
      txtLevel1 = (TextView) itemView.findViewById(R.id.txt_level_1);
      txtSubtitle = (TextView) itemView.findViewById(R.id.txt_subtitle);
    }

    @Override
    public void onBind(final MESSAGE message) {
      super.onBind(message);

      final InfoBlock data = message.getRankingInfo();
      if (data == null) {
        itemView.setVisibility(View.GONE);
        return;
      }

      String targetName = data.getReportName();
      if (targetName == null || targetName.isEmpty())
        valueTv.setVisibility(View.GONE);
      else {
        valueTv.setVisibility(View.VISIBLE);
        valueTv.setText((targetName));
      }

      if (data.getUnit() != null && !data.getUnit().isEmpty()) {
        unitTv.setVisibility(View.VISIBLE);
        unitTv.setText(data.getUnit());
      } else {
        unitTv.setVisibility(View.GONE);
      }

      List<InfoBlock.RankingInfoMainBlock> rankingBlock = data.getRankingBlock();
      if (rankingBlock != null && rankingBlock.size() > 0) {
        infoBlockMainXepHang.setVisibility(View.VISIBLE);
        InfoBlock.RankingInfoMainBlock mainBlock1 = null;
        InfoBlock.RankingInfoMainBlock mainBlock2 = null;
        if (rankingBlock.size() == 1) {
          infoBlockMainXepHang1.setVisibility(View.GONE);
          mainBlock1 = rankingBlock.get(0);
          txtUnit2.setTextSize(14);
          txtInfoBlockMainRank2.setTextSize(23);
        } else if (rankingBlock.size() == 2) {
          infoBlockMainXepHang1.setVisibility(View.VISIBLE);
          mainBlock1 = rankingBlock.get(1);
          mainBlock2 = rankingBlock.get(0);
          txtUnit2.setTextSize(13);
          txtUnit1.setTextSize(13);
          txtInfoBlockMainRank2.setTextSize(19);
          txtInfoBlockMainRank1.setTextSize(19);
        }

        if (mainBlock1 != null) {
          if (!StringUtils.isEmpty(mainBlock1.getUnit())) {
            txtUnit2.setVisibility(View.VISIBLE);
            txtUnit2.setText(mainBlock1.getUnit());
          } else txtUnit2.setVisibility(View.GONE);

          if (!StringUtils.isEmpty(mainBlock1.getLevel())) {
            txtLevel2.setVisibility(View.VISIBLE);
            txtLevel2.setText(mainBlock1.getLevel());
          } else txtLevel2.setVisibility(View.GONE);

          txtInfoBlockMainRank2.setText(mainBlock1.getCurrentRank());
          txtYesterdayRank2.setText(mainBlock1.getLastRank());
          int delta = 0;
          try {
            delta = Integer.parseInt(mainBlock1.getDeltaRank().trim());
          } catch (Exception e) {
            Logger.log(e);
            delta = 0;
          }
          if (delta < 0) {
            imgDelta2.setImageResource(R.drawable.ic_triangle_down_red);
          } else if (delta > 0) {
            imgDelta2.setImageResource(R.drawable.ic_triangle_up_green);
          } else {
            imgDelta2.setImageResource(R.drawable.ic_keep_delta);
          }
        }
        if (mainBlock2 != null) {
          if (!StringUtils.isEmpty(mainBlock2.getUnit())) {
            txtUnit1.setVisibility(View.VISIBLE);
            txtUnit1.setText(mainBlock2.getUnit());
          } else txtUnit1.setVisibility(View.GONE);

          if (!StringUtils.isEmpty(mainBlock2.getLevel())) {
            txtLevel1.setVisibility(View.VISIBLE);
            txtLevel1.setText(mainBlock2.getLevel());
          } else txtLevel1.setVisibility(View.GONE);

          txtInfoBlockMainRank1.setText(mainBlock2.getCurrentRank());
          txtYesterdayRank1.setText(mainBlock2.getLastRank());

          int delta = 0;
          try {
            delta = Integer.parseInt(mainBlock2.getDeltaRank().trim());
          } catch (Exception e) {
            Logger.log(e);
            delta = 0;
          }
          if (delta < 0) {
            imgDelta1.setImageResource(R.drawable.ic_triangle_down_red);
          } else if (delta > 0) {
            imgDelta1.setImageResource(R.drawable.ic_triangle_up_green);
          } else {
            imgDelta1.setImageResource(R.drawable.ic_keep_delta);
          }
        }

      } else {
        infoBlockMainXepHang.setVisibility(View.GONE);
      }

      RecyclerUtils.setupGridRecyclerView(itemView.getContext(), subBlockInfoRv, 2);
      if (data.getOtherInfo() != null) {
        subBlockInfoRv.setVisibility(View.VISIBLE);
        subBlockInfoRv.setAdapter(new AreaDetailsAdapter(data.getOtherInfo()));
        subBlockInfoRv.setAdapter(new AreaDetailsAdapter(data.getOtherInfo()));

      } else {
        subBlockInfoRv.setVisibility(View.GONE);
      }

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (onItemViewClick != null) onItemViewClick.onItemChartClick(message.getPayload());
        }
      });

      if (time != null) time.setVisibility(View.GONE);

    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }


  public static class IncomingLineChartViewHolder<MESSAGE extends MessageContentType.LineChart>
      extends BaseIncomingChartMessageViewHolder<MESSAGE> {

    public IncomingLineChartViewHolder(View itemView) {
      super(itemView);
    }

    public void onBind(final MESSAGE message) {
      super.onBind(message);

      mLegendRv.setVisibility(View.VISIBLE);
      ItemLineChartServiceDTO itemLineChart = message.getItemLineChart();
      mNameTv.setText(itemLineChart.getChartTitle());
      mUnitTv.setText(itemLineChart.getValueUnit());
      LineChart mLineChart = new LineChart(itemView.getContext());
      RelativeLayout.LayoutParams chartParams = new RelativeLayout
          .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ChatHeadUtils.dpToPx(itemView.getContext(), 200));
      mLineChart.setLayoutParams(chartParams);

      mChartContainer.addView(mLineChart);
      LineChartUtils.bindServiceDataAreaChart(mLineChart, itemLineChart.getLineData(), itemLineChart.getDates(),
          itemLineChart.getListLegends(), mLegendRv, null);
      LineChartUtils.setupActionHoldLineChart(mLineChart, mChartContainer, itemLineChart);
      mLineChart.setMarker(null);
      mLineChart.getXAxis().setDrawLabels(false);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (onItemViewClick != null) onItemViewClick.onItemChartClick(message.getPayload());
        }
      });

      if (time != null) time.setVisibility(View.GONE);

    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  public static class IncomingHorizontalRankChartViewHolder<MESSAGE extends MessageContentType.HorizontalRankChart>
      extends BaseIncomingChartMessageViewHolder<MESSAGE> {
    private LegendsAdapter.OnItemLegendClickListener onItemLegendClickListener;

    public IncomingHorizontalRankChartViewHolder(View itemView) {
      super(itemView);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      ItemHorizontalStackRankingUnitData data = message.getItemHorizontalStackRankingChart();
      if (null == data || data.getChartData() == null)
        return;
      tvPercent.setVisibility(View.VISIBLE);
      mNameTv.setText(data.getChartData().getChartName());
      mUnitTv.setText(data.getChartData().getUnit());
      final HorizontalBarChart mBarChart = new HorizontalBarChart(itemView.getContext());
      RelativeLayout.LayoutParams chartParams = new RelativeLayout
          .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
          ChatHeadUtils.dpToPx(itemView.getContext(), 180));
      mBarChart.setLayoutParams(chartParams);

      mBarChart.setDrawValueAboveBar(false);

      //bind data  chart
      mChartContainer.addView(mBarChart);

      BarChartUtils.bindServiceDataHorizontalStackColumnChart(ViewType.PERCENT, mBarChart, 0, 100, data.getChartData(),
          mLegendRv, onItemLegendClickListener);

      BarChartUtils.setupActionHoldHorizontalColumnChart(mBarChart, mChartContainer, data.getChartData(), false);
      mBarChart.getAxisLeft().setDrawLabels(false);

      mBarChart.getLegend().setEnabled(false);
      mBarChart.getAxisRight().setDrawLabels(true);
      mBarChart.getAxisRight().setDrawZeroLine(true);
      mBarChart.getAxisRight().setZeroLineColor(Color.BLACK);

      mBarChart.setContentDescription("");
      mLegendRv.setVisibility(View.GONE);
      mUnitTv.setVisibility(View.GONE);

      layoutContent.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.white));

      if (time != null) time.setVisibility(View.GONE);
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  public static class IncomingAvailableFeaturesViewHolder<MESSAGE extends MessageContentType.AvailableFeatureType>
      extends BaseIncomingMessageViewHolder<MESSAGE> {
    public RecyclerView mRecycler;

    public IncomingAvailableFeaturesViewHolder(View itemView) {
      super(itemView);

      mRecycler = (RecyclerView) itemView.findViewById(R.id.recycler);
      mRecycler.setHasFixedSize(true);
      LinearLayoutManager layoutManager = new LinearLayoutManager(context);
      layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//      DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());
//      dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider_horizonal));
//      mRecycler.addItemDecoration(dividerItemDecoration);
      mRecycler.setLayoutManager(layoutManager);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);

      AvailableFeatureAdapter adapter = new AvailableFeatureAdapter(message.getAvailableFeatures(), onItemAvailableFeatureClick);
      mRecycler.setAdapter(adapter);

      if (time != null) time.setVisibility(View.GONE);
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getIncomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
      }
    }
  }

  public static class IncomingConclusionMessageViewHolder<MESSAGE extends MessageContentType.ConclusionType>
      extends BaseIncomingMessageViewHolder<MESSAGE> {
    private TextView txtTitle;
    private TextView txtDescription;
    private ImageView imgIcon;
    private TextView txtFooter;

    public IncomingConclusionMessageViewHolder(View itemView) {
      super(itemView);
      txtTitle = (TextView) itemView.findViewById(R.id.txt_title_conclusion);
      txtDescription = (TextView) itemView.findViewById(R.id.txt_description_conclusion);
      imgIcon = (ImageView) itemView.findViewById(R.id.img_icon_conclusion);
      txtFooter = (TextView) itemView.findViewById(R.id.txt_footer);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);

      final ConclusionMessage conclusionMessage = message.getConclusionMessage();
      txtTitle.setText(conclusionMessage.getTitle());
      txtDescription.setText(conclusionMessage.getDescription());
      txtFooter.setText(conclusionMessage.getFooter());

      Glide.with(itemView.getContext())
          .load(conclusionMessage.getIcon()).into(imgIcon);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (onItemViewClick != null) onItemViewClick.onItemConclusionClick(conclusionMessage);
        }
      });
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  public abstract static class BaseIncomingChartMessageViewHolder<MESSAGE extends IMessage>
      extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

    protected TextView time;
    protected ImageView userAvatar;

    public TextView mNameTv;
    public TextView mFilterTv;
    public TextView mUnitTv;
    public TextView mUnitRightTv;
    public RecyclerView mLegendRv;
    public RelativeLayout mChartContainer;
    public RelativeLayout mWaterMarkContainer;
    public TextView tvPercent;
    public RecyclerView mViewTypeRv;
    //    public ImageView mMenuImg;
//    public ImageView mZoomMenuTv;
//    public ImageView mInfoServiceDefineImg;
//    public ImageView mItemPinDashboard;
    public View layoutDraggable;
    public View layoutContent;

    public BaseIncomingChartMessageViewHolder(View itemView) {
      super(itemView);
      time = (TextView) itemView.findViewById(R.id.messageTime);
      userAvatar = (ImageView) itemView.findViewById(R.id.messageUserAvatar);
      mNameTv = (TextView) itemView.findViewById(R.id.item_services_chart_name_tv);
      mFilterTv = (TextView) itemView.findViewById(R.id.item_services_chart_filter_tv);
      mUnitTv = (TextView) itemView.findViewById(R.id.item_service_chart_unit_tv);
      mUnitRightTv = (TextView) itemView.findViewById(R.id.item_service_chart_unit_right_tv);
      mLegendRv = (RecyclerView) itemView.findViewById(R.id.item_services_chart_legend);
      mChartContainer = (RelativeLayout) itemView.findViewById(R.id.item_services_chart_container);
      mWaterMarkContainer = (RelativeLayout) itemView.findViewById(R.id.water_mark_chart_container);
      tvPercent = (TextView) itemView.findViewById(R.id.xephang_don_vi_tv);
      mViewTypeRv = (RecyclerView) itemView.findViewById(R.id.item_services_chart_view_type_recycler_view);
//      mMenuImg = (ImageView) itemView.findViewById(R.id.item_service_menu_img);
//      mZoomMenuTv = (ImageView) itemView.findViewById(R.id.item_info_zoom_menu_tv);
//      mInfoServiceDefineImg = (ImageView) itemView.findViewById(R.id.item_info_service_definition);
//      mItemPinDashboard = (ImageView) itemView.findViewById(R.id.item_pin_dashboard);
      layoutDraggable = itemView.findViewById(R.id.layout_draggable);
      layoutContent = itemView.findViewById(R.id.layout_content);
    }

    @Override
    public void onBind(MESSAGE message) {
      mChartContainer.removeAllViews();

      if (time != null) {
        time.setVisibility(View.VISIBLE);
        time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
      }

      if (userAvatar != null) {
        boolean isAvatarExists = imageLoader != null
            && message.getUser().getAvatar() != null
            && !message.getUser().getAvatar().isEmpty();

        userAvatar.setVisibility(isAvatarExists ? View.VISIBLE : View.GONE);
        if (isAvatarExists) {
          int resId;
          try {
            resId = Integer.parseInt(message.getUser().getAvatar());
          } catch (Exception e) {
            Logger.log(e);
            resId = R.drawable.ic_einstein_chat_head;
          }
          Glide.with(itemView.getContext()).load(resId).into(userAvatar);

        }
      }
    }

    @Override
    public void applyStyle(MessagesListStyle style) {
      if (time != null) {
        time.setTextColor(style.getIncomingTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingTimeTextStyle());
      }

      if (userAvatar != null) {
        userAvatar.getLayoutParams().width = style.getIncomingAvatarWidth();
        userAvatar.getLayoutParams().height = style.getIncomingAvatarHeight();
      }

    }
  }

  public static class IncomingImageLinkMessageViewHolder<MESSAGE extends MessageContentType.Link>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    protected ImageView image;
    protected View imageOverlay;

    public IncomingImageLinkMessageViewHolder(View itemView) {
      super(itemView);
      image = (ImageView) itemView.findViewById(R.id.image);
      imageOverlay = itemView.findViewById(R.id.imageOverlay);
      image.setVisibility(View.VISIBLE);

      if (image != null && image instanceof RoundedImageView) {
        ((RoundedImageView) image).setCorners(
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            0
        );
      }
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (image != null && imageLoader != null) {
        imageLoader.loadImage(image, message.getLinkImage());
      }

      if (imageOverlay != null) {
        imageOverlay.setSelected(isSelected());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getIncomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingImageTimeTextStyle());
      }

      if (imageOverlay != null) {
        ViewCompat.setBackground(imageOverlay, style.getIncomingImageOverlayDrawable());
      }
    }
  }

  public static class OutcomingImageLinkMessageViewHolder<MESSAGE extends MessageContentType.Link>
      extends BaseOutcomingMessageViewHolder<MESSAGE> {

    protected ImageView image;
    protected View imageOverlay;

    public OutcomingImageLinkMessageViewHolder(View itemView) {
      super(itemView);
      image = (ImageView) itemView.findViewById(R.id.image);
      imageOverlay = itemView.findViewById(R.id.imageOverlay);
      image.setVisibility(View.VISIBLE);

      if (image != null && image instanceof RoundedImageView) {
        ((RoundedImageView) image).setCorners(
            R.dimen.message_bubble_corners_radius,
            R.dimen.message_bubble_corners_radius,
            0,
            R.dimen.message_bubble_corners_radius
        );
      }
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);
      if (image != null && imageLoader != null) {
        imageLoader.loadImage(image, message.getLinkImage());
      }

      if (imageOverlay != null) {
        imageOverlay.setSelected(isSelected());
      }
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
      if (time != null) {
        time.setTextColor(style.getOutcomingImageTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getOutcomingImageTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getOutcomingImageTimeTextStyle());
      }

      if (imageOverlay != null) {
        ViewCompat.setBackground(imageOverlay, style.getOutcomingImageOverlayDrawable());
      }
    }
  }


  public static class IncomingActionMessageViewHolder<MESSAGE extends MessageContentType.ActionList>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    public TextView mTvTitle;
    public RecyclerView mRecycler;

    public IncomingActionMessageViewHolder(View itemView) {
      super(itemView);
      mTvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
      mRecycler = (RecyclerView) itemView.findViewById(R.id.recycler);
      mRecycler.setHasFixedSize(true);
      LinearLayoutManager layoutManager = new LinearLayoutManager(context);
      layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
      DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());
      dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider_horizonal));
      mRecycler.addItemDecoration(dividerItemDecoration);
      mRecycler.setLayoutManager(layoutManager);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);

      ButtonListAdapter adapter = new ButtonListAdapter(context, message.getButtonList(), onMessageButtonClickListener);
      mRecycler.setAdapter(adapter);
      adapter.notifyDataSetChanged();

      mTvTitle.setText(message.getText());
    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  public static class IncomingQuickReplyMessageViewHolder<MESSAGE extends MessageContentType.QuickReply>
      extends BaseIncomingMessageViewHolder<MESSAGE> {

    public TextView mTvTitle;
    public RecyclerView mRecycler;

    public IncomingQuickReplyMessageViewHolder(View itemView) {
      super(itemView);
      mTvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
      mRecycler = (RecyclerView) itemView.findViewById(R.id.recycler);
      mRecycler.setHasFixedSize(true);
      LinearLayoutManager layoutManager = new LinearLayoutManager(context);
      layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//      DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, layoutManager.getOrientation());
//      dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.divider_horizonal));
//      mRecycler.addItemDecoration(dividerItemDecoration);
      mRecycler.setLayoutManager(layoutManager);
    }

    @Override
    public void onBind(MESSAGE message) {
      super.onBind(message);

      ActionListAdapter adapter = new ActionListAdapter(context, message.getActionList(), onMessageActionClickListener,
          this.getAdapterPosition());
      mRecycler.setAdapter(adapter);
      adapter.notifyDataSetChanged();

//            if (message.getText()!=null){
//              mTvTitle.setVisibility(View.VISIBLE);
//              mTvTitle.setText(message.getText());
//            } else {
//              mTvTitle.setVisibility(View.GONE);
//            }

    }

    @Override
    public final void applyStyle(MessagesListStyle style) {
      super.applyStyle(style);
    }
  }

  /**
   * Default view holder implementation for date header
   */
  public static class DefaultDateHeaderViewHolder extends ViewHolder<Date>
      implements DefaultMessageViewHolder {

    protected TextView text;
    protected String dateFormat;
    protected DateFormatter.Formatter dateHeadersFormatter;

    public DefaultDateHeaderViewHolder(View itemView) {
      super(itemView);
      text = (TextView) itemView.findViewById(R.id.messageText);
    }

    @Override
    public void onBind(Date date) {
      if (text != null) {
        String formattedDate = null;
        if (dateHeadersFormatter != null) formattedDate = dateHeadersFormatter.format(date);
        if (dateFormat == null) {
          dateFormat = DateFormatter.Template.STRING_DAY_MONTH_YEAR.get();
        }
        text.setText(formattedDate == null ? DateFormatter.format(date, dateFormat) : formattedDate);
      }
    }

    @Override
    public void applyStyle(MessagesListStyle style) {
      if (text != null) {
        text.setTextColor(style.getDateHeaderTextColor());
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getDateHeaderTextSize());
        text.setTypeface(text.getTypeface(), style.getDateHeaderTextStyle());
        text.setPadding(style.getDateHeaderPadding(), style.getDateHeaderPadding(),
            style.getDateHeaderPadding(), style.getDateHeaderPadding());
      }
      dateFormat = style.getDateHeaderFormat();
      dateFormat = dateFormat == null ? DateFormatter.Template.STRING_DAY_MONTH_YEAR.get() : dateFormat;
    }
  }

  /**
   * Base view holder for incoming message
   */
  public abstract static class BaseIncomingMessageViewHolder<MESSAGE extends IMessage>
      extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

    protected TextView time;
    protected ImageView userAvatar;

    public BaseIncomingMessageViewHolder(View itemView) {
      super(itemView);
      time = (TextView) itemView.findViewById(R.id.messageTime);
      userAvatar = (ImageView) itemView.findViewById(R.id.messageUserAvatar);
    }

    @Override
    public void onBind(MESSAGE message) {
      if (time != null) {
        time.setVisibility(View.VISIBLE);
        time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
      }

      if (userAvatar != null) {
        boolean isAvatarExists = message.getUser().getAvatar() != null
            && !message.getUser().getAvatar().isEmpty();

        userAvatar.setVisibility(isAvatarExists ? View.VISIBLE : View.GONE);
        if (isAvatarExists) {
          int resId;
          try {
            resId = Integer.parseInt(message.getUser().getAvatar());
          } catch (Exception e) {
            Logger.log(e);
            resId = R.drawable.ic_einstein_chat_head;
          }
          Glide.with(itemView.getContext()).load(resId).into(userAvatar);
        }
      }
    }

    @Override
    public void applyStyle(MessagesListStyle style) {
      if (time != null) {
        time.setTextColor(style.getIncomingTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getIncomingTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getIncomingTimeTextStyle());
      }

      if (userAvatar != null) {
        userAvatar.getLayoutParams().width = style.getIncomingAvatarWidth();
        userAvatar.getLayoutParams().height = style.getIncomingAvatarHeight();
      }

    }
  }

  /**
   * Base view holder for outcoming message
   */
  public abstract static class BaseOutcomingMessageViewHolder<MESSAGE extends IMessage>
      extends BaseMessageViewHolder<MESSAGE> implements DefaultMessageViewHolder {

    protected TextView time;

    public BaseOutcomingMessageViewHolder(View itemView) {
      super(itemView);
      time = (TextView) itemView.findViewById(R.id.messageTime);
    }

    @Override
    public void onBind(MESSAGE message) {
      if (time != null) {
        time.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
      }
    }

    @Override
    public void applyStyle(MessagesListStyle style) {
      if (time != null) {
        time.setTextColor(style.getOutcomingTimeTextColor());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, style.getOutcomingTimeTextSize());
        time.setTypeface(time.getTypeface(), style.getOutcomingTimeTextStyle());
      }
    }
  }

    /*
    * DEFAULTS
    * */

  interface DefaultMessageViewHolder {
    void applyStyle(MessagesListStyle style);
  }

  private static class DefaultIncomingTextMessageViewHolder
      extends IncomingTextMessageViewHolder<IMessage> {

    public DefaultIncomingTextMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultOutcomingTextMessageViewHolder
      extends OutcomingTextMessageViewHolder<IMessage> {

    public DefaultOutcomingTextMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingImageMessageViewHolder
      extends IncomingImageMessageViewHolder<MessageContentType.Image> {

    public DefaultIncomingImageMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultOutcomingImageMessageViewHolder
      extends OutcomingImageMessageViewHolder<MessageContentType.Image> {

    public DefaultOutcomingImageMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingImageLoadingViewHolder
      extends IncomingImageLoadingViewHolder<MessageContentType.ImageLoading> {

    public DefaultIncomingImageLoadingViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingBlockInfoViewHolder
      extends IncomingBlockInfoViewHolder<MessageContentType.BlockInfo> {

    public DefaultIncomingBlockInfoViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingRankingInfoViewHolder
      extends IncomingRankingInfoViewHolder<MessageContentType.RankingInfoType> {

    public DefaultIncomingRankingInfoViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingLineChartViewHolder
      extends IncomingLineChartViewHolder<MessageContentType.LineChart> {

    public DefaultIncomingLineChartViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingHorizontalRankChartViewHolder
      extends IncomingHorizontalRankChartViewHolder<MessageContentType.HorizontalRankChart> {

    public DefaultIncomingHorizontalRankChartViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingImageLinkMessageViewHolder
      extends IncomingImageLinkMessageViewHolder<MessageContentType.Link> {

    public DefaultIncomingImageLinkMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultOutcomingImageLinkMessageViewHolder
      extends OutcomingImageLinkMessageViewHolder<MessageContentType.Link> {

    public DefaultOutcomingImageLinkMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingActionMessageViewHolder
      extends IncomingActionMessageViewHolder<MessageContentType.ActionList> {

    public DefaultIncomingActionMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultOutcomingActionMessageViewHolder
      extends IncomingActionMessageViewHolder<MessageContentType.ActionList> {

    public DefaultOutcomingActionMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingQuickReplyMessageViewHolder
      extends IncomingQuickReplyMessageViewHolder<MessageContentType.QuickReply> {

    public DefaultIncomingQuickReplyMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingAvailableFeaturesViewHolder
      extends IncomingAvailableFeaturesViewHolder<MessageContentType.AvailableFeatureType> {

    public DefaultIncomingAvailableFeaturesViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultIncomingConclusionMessageViewHolder
      extends IncomingConclusionMessageViewHolder<MessageContentType.ConclusionType> {

    public DefaultIncomingConclusionMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  private static class DefaultOutcomingQuickReplyMessageViewHolder
      extends IncomingQuickReplyMessageViewHolder<MessageContentType.QuickReply> {

    public DefaultOutcomingQuickReplyMessageViewHolder(View itemView) {
      super(itemView);
    }
  }

  public interface OnItemViewClick {
    void onItemConclusionClick(ConclusionMessage conclusionMessage);

    void onItemChartClick(Object payload);
  }

  private static class ContentTypeConfig<TYPE extends MessageContentType> {

    private byte type;
    private HolderConfig<TYPE> incomingConfig;
    private HolderConfig<TYPE> outcomingConfig;

    private ContentTypeConfig(
        byte type, HolderConfig<TYPE> incomingConfig, HolderConfig<TYPE> outcomingConfig) {

      this.type = type;
      this.incomingConfig = incomingConfig;
      this.outcomingConfig = outcomingConfig;
    }
  }

  protected class HolderConfig<T extends IMessage> {

    protected Class<? extends BaseMessageViewHolder<? extends T>> holder;
    protected int layout;

    public HolderConfig(Class<? extends BaseMessageViewHolder<? extends T>> holder, int layout) {
      this.holder = holder;
      this.layout = layout;
    }
  }
}
