package com.vtt.chatlib.chatkit.utils;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by HaiKE on 10/25/17.
 */

public class MyValueFormatter implements IValueFormatter {

  private DecimalFormat mFormat;

  public MyValueFormatter() {
    mFormat = new DecimalFormat("###,###,###,##0.0");
  }

  @Override
  public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
    return mFormat.format(value) + " đ";
  }
}
