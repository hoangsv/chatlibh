package com.vtt.chatlib.chatkit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HaiKE on 10/25/17.
 */

public class ChartStack {
  @SerializedName("blockId")
  @Expose
  private Integer blockId;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("id")
  @Expose
  private Integer id;
  private final static long serialVersionUID = -8986724527855507375L;

  public Integer getBlockId() {
    return blockId;
  }

  public void setBlockId(Integer blockId) {
    this.blockId = blockId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Example{" +
        "blockId=" + blockId +
        ", type='" + type + '\'' +
        ", data=" + data +
        ", id=" + id +
        '}';
  }

  public class ChartData implements Serializable {

    @SerializedName("viewMore")
    @Expose
    private Boolean viewMore;
    @SerializedName("chartName")
    @Expose
    private String chartName;
    @SerializedName("unit")
    @Expose
    private Object unit;
    @SerializedName("objectType")
    @Expose
    private String objectType;
    @SerializedName("columnPieceStyles")
    @Expose
    private List<ColumnPieceStyle> columnPieceStyles = null;
    @SerializedName("columns")
    @Expose
    private List<Column> columns = null;
    private final static long serialVersionUID = 2058783415807491739L;

    public Boolean getViewMore() {
      return viewMore;
    }

    public void setViewMore(Boolean viewMore) {
      this.viewMore = viewMore;
    }

    public String getChartName() {
      return chartName;
    }

    public void setChartName(String chartName) {
      this.chartName = chartName;
    }

    public Object getUnit() {
      return unit;
    }

    public void setUnit(Object unit) {
      this.unit = unit;
    }

    public String getObjectType() {
      return objectType;
    }

    public void setObjectType(String objectType) {
      this.objectType = objectType;
    }

    public List<ColumnPieceStyle> getColumnPieceStyles() {
      return columnPieceStyles;
    }

    public void setColumnPieceStyles(List<ColumnPieceStyle> columnPieceStyles) {
      this.columnPieceStyles = columnPieceStyles;
    }

    public List<Column> getColumns() {
      return columns;
    }

    public void setColumns(List<Column> columns) {
      this.columns = columns;
    }

    @Override
    public String toString() {
      return "ChartData{" +
          "viewMore=" + viewMore +
          ", chartName='" + chartName + '\'' +
          ", unit=" + unit +
          ", objectType='" + objectType + '\'' +
          ", columnPieceStyles=" + columnPieceStyles +
          ", columns=" + columns +
          '}';
    }
  }

  public class Column implements Serializable {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("percent")
    @Expose
    private Object percent;
    @SerializedName("dataColumnPieces")
    @Expose
    private List<DataColumnPiece> dataColumnPieces = null;
    @SerializedName("event")
    @Expose
    private Object event;
    private final static long serialVersionUID = 2946230098647313234L;

    public String getLabel() {
      return label;
    }

    public void setLabel(String label) {
      this.label = label;
    }

    public Integer getValue() {
      return value;
    }

    public void setValue(Integer value) {
      this.value = value;
    }

    public Object getPercent() {
      return percent;
    }

    public void setPercent(Object percent) {
      this.percent = percent;
    }

    public List<DataColumnPiece> getDataColumnPieces() {
      return dataColumnPieces;
    }

    public void setDataColumnPieces(List<DataColumnPiece> dataColumnPieces) {
      this.dataColumnPieces = dataColumnPieces;
    }

    public Object getEvent() {
      return event;
    }

    public void setEvent(Object event) {
      this.event = event;
    }

    @Override
    public String toString() {
      return "Column{" +
          "label='" + label + '\'' +
          ", value=" + value +
          ", percent=" + percent +
          ", dataColumnPieces=" + dataColumnPieces +
          ", event=" + event +
          '}';
    }
  }

  public class ColumnPieceStyle implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("objectId")
    @Expose
    private String objectId;
    private final static long serialVersionUID = 2054501783497060029L;

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public String getColor() {
      return color;
    }

    public void setColor(String color) {
      this.color = color;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getObjectId() {
      return objectId;
    }

    public void setObjectId(String objectId) {
      this.objectId = objectId;
    }

    @Override
    public String toString() {
      return "ColumnPieceStyle{" +
          "id=" + id +
          ", color='" + color + '\'' +
          ", title='" + title + '\'' +
          ", objectId='" + objectId + '\'' +
          '}';
    }
  }

  public class Data implements Serializable {

    @SerializedName("viewTypes")
    @Expose
    private List<ViewType> viewTypes = null;
    @SerializedName("currentType")
    @Expose
    private Object currentType;
    @SerializedName("asc")
    @Expose
    private Boolean asc;
    @SerializedName("chartData")
    @Expose
    private ChartData chartData;
    @SerializedName("filterMetadata")
    @Expose
    private FilterMetadata filterMetadata;
    private final static long serialVersionUID = -7501021848011760463L;

    public List<ViewType> getViewTypes() {
      return viewTypes;
    }

    public void setViewTypes(List<ViewType> viewTypes) {
      this.viewTypes = viewTypes;
    }

    public Object getCurrentType() {
      return currentType;
    }

    public void setCurrentType(Object currentType) {
      this.currentType = currentType;
    }

    public Boolean getAsc() {
      return asc;
    }

    public void setAsc(Boolean asc) {
      this.asc = asc;
    }

    public ChartData getChartData() {
      return chartData;
    }

    public void setChartData(ChartData chartData) {
      this.chartData = chartData;
    }

    public FilterMetadata getFilterMetadata() {
      return filterMetadata;
    }

    public void setFilterMetadata(FilterMetadata filterMetadata) {
      this.filterMetadata = filterMetadata;
    }

    @Override
    public String toString() {
      return "Data{" +
          "viewTypes=" + viewTypes +
          ", currentType=" + currentType +
          ", asc=" + asc +
          ", chartData=" + chartData +
          ", filterMetadata=" + filterMetadata +
          '}';
    }
  }

  public class DataColumnPiece implements Serializable {

    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("percent")
    @Expose
    private Double percent;
    @SerializedName("styleId")
    @Expose
    private Integer styleId;
    private final static long serialVersionUID = 6175912842223922417L;

    public Integer getValue() {
      return value;
    }

    public void setValue(Integer value) {
      this.value = value;
    }

    public Double getPercent() {
      return percent;
    }

    public void setPercent(Double percent) {
      this.percent = percent;
    }

    public Integer getStyleId() {
      return styleId;
    }

    public void setStyleId(Integer styleId) {
      this.styleId = styleId;
    }

    @Override
    public String toString() {
      return "DataColumnPiece{" +
          "value=" + value +
          ", percent=" + percent +
          ", styleId=" + styleId +
          '}';
    }
  }

  public class FilterMetadata implements Serializable {

    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("ranking")
    @Expose
    private Object ranking;
    @SerializedName("toDate")
    @Expose
    private Integer toDate;
    @SerializedName("fromDate")
    @Expose
    private Object fromDate;
    @SerializedName("unit")
    @Expose
    private Unit unit;
    @SerializedName("cycleTime")
    @Expose
    private String cycleTime;
    @SerializedName("areaCode")
    @Expose
    private Object areaCode;
    @SerializedName("areaLevel")
    @Expose
    private String areaLevel;
    @SerializedName("relativyTime")
    @Expose
    private Object relativyTime;
    @SerializedName("channelId")
    @Expose
    private Object channelId;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("subService")
    @Expose
    private Integer subService;
    @SerializedName("eventDesc")
    @Expose
    private Object eventDesc;
    private final static long serialVersionUID = -2460351059225780820L;

    public String getServiceId() {
      return serviceId;
    }

    public void setServiceId(String serviceId) {
      this.serviceId = serviceId;
    }

    public String getServiceName() {
      return serviceName;
    }

    public void setServiceName(String serviceName) {
      this.serviceName = serviceName;
    }

    public Object getRanking() {
      return ranking;
    }

    public void setRanking(Object ranking) {
      this.ranking = ranking;
    }

    public Integer getToDate() {
      return toDate;
    }

    public void setToDate(Integer toDate) {
      this.toDate = toDate;
    }

    public Object getFromDate() {
      return fromDate;
    }

    public void setFromDate(Object fromDate) {
      this.fromDate = fromDate;
    }

    public Unit getUnit() {
      return unit;
    }

    public void setUnit(Unit unit) {
      this.unit = unit;
    }

    public String getCycleTime() {
      return cycleTime;
    }

    public void setCycleTime(String cycleTime) {
      this.cycleTime = cycleTime;
    }

    public Object getAreaCode() {
      return areaCode;
    }

    public void setAreaCode(Object areaCode) {
      this.areaCode = areaCode;
    }

    public String getAreaLevel() {
      return areaLevel;
    }

    public void setAreaLevel(String areaLevel) {
      this.areaLevel = areaLevel;
    }

    public Object getRelativyTime() {
      return relativyTime;
    }

    public void setRelativyTime(Object relativyTime) {
      this.relativyTime = relativyTime;
    }

    public Object getChannelId() {
      return channelId;
    }

    public void setChannelId(Object channelId) {
      this.channelId = channelId;
    }

    public String getServiceType() {
      return serviceType;
    }

    public void setServiceType(String serviceType) {
      this.serviceType = serviceType;
    }

    public Integer getSubService() {
      return subService;
    }

    public void setSubService(Integer subService) {
      this.subService = subService;
    }

    public Object getEventDesc() {
      return eventDesc;
    }

    public void setEventDesc(Object eventDesc) {
      this.eventDesc = eventDesc;
    }

    @Override
    public String toString() {
      return "FilterMetadata{" +
          "serviceId='" + serviceId + '\'' +
          ", serviceName='" + serviceName + '\'' +
          ", ranking=" + ranking +
          ", toDate=" + toDate +
          ", fromDate=" + fromDate +
          ", unit=" + unit +
          ", cycleTime='" + cycleTime + '\'' +
          ", areaCode=" + areaCode +
          ", areaLevel='" + areaLevel + '\'' +
          ", relativyTime=" + relativyTime +
          ", channelId=" + channelId +
          ", serviceType='" + serviceType + '\'' +
          ", subService=" + subService +
          ", eventDesc=" + eventDesc +
          '}';
    }
  }

  public class Unit implements Serializable {

    @SerializedName("province")
    @Expose
    private Object province;
    @SerializedName("district")
    @Expose
    private Object district;
    @SerializedName("station")
    @Expose
    private Object station;
    private final static long serialVersionUID = -3111737835592142544L;

    public Object getProvince() {
      return province;
    }

    public void setProvince(Object province) {
      this.province = province;
    }

    public Object getDistrict() {
      return district;
    }

    public void setDistrict(Object district) {
      this.district = district;
    }

    public Object getStation() {
      return station;
    }

    public void setStation(Object station) {
      this.station = station;
    }

    @Override
    public String toString() {
      return "Unit{" +
          "province=" + province +
          ", district=" + district +
          ", station=" + station +
          '}';
    }
  }

  public class ViewType implements Serializable {

    @SerializedName("objectID")
    @Expose
    private String objectID;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;
    private final static long serialVersionUID = -1391667439886913920L;

    public String getObjectID() {
      return objectID;
    }

    public void setObjectID(String objectID) {
      this.objectID = objectID;
    }

    public String getDisplayValue() {
      return displayValue;
    }

    public void setDisplayValue(String displayValue) {
      this.displayValue = displayValue;
    }

    @Override
    public String toString() {
      return "ViewType{" +
          "objectID='" + objectID + '\'' +
          ", displayValue='" + displayValue + '\'' +
          '}';
    }
  }
}

