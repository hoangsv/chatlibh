package com.vtt.chatlib.chatkit.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by HaiKE on 10/25/17.
 */

public class MyAxisValueFormatter implements IAxisValueFormatter {

  private DecimalFormat mFormat;

  public MyAxisValueFormatter() {
    mFormat = new DecimalFormat("###,###,###,##0.0");
  }

  @Override
  public String getFormattedValue(float value, AxisBase axis) {
    return mFormat.format(value) + " đ";
  }

  @Override
  public String getFullValue(float value, AxisBase axis) {
    return String.valueOf(value) + " đ";
  }
}
