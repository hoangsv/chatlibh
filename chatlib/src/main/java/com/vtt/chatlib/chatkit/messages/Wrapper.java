package com.vtt.chatlib.chatkit.messages;

/**
 * Created by hungl on 07-Nov-17.
 * ${CLASS}
 */


    /*
    * WRAPPER
    * */
public class Wrapper<DATA> {
  protected DATA item;
  protected boolean isSelected;

  public Wrapper(DATA item) {
    this.item = item;
  }

  public DATA getItem() {
    return item;
  }
}
