package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by HaiKE on 10/18/17.
 */

public class MessageRequest implements Serializable {

  @SerializedName("text")
  private String text;
  @SerializedName("type")
  private String type;
  @SerializedName("payload")
  private Object payload;
  @SerializedName("time")
  private String time;

  private final static long serialVersionUID = -4656431255772315621L;

  public MessageRequest(String text, String type, String time, Object payload) {
    this.text = text;
    this.type = type;
    this.time = time;
    this.payload = payload;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  @Override
  public String toString() {
    return "MessageRequest{" +
        "value='" + text + '\'' +
        ", type='" + type + '\'' +
        '}';
  }
}