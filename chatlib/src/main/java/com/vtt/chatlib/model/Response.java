package com.vtt.chatlib.model;

import com.google.gson.annotations.SerializedName;
import com.vtt.chatlib.chatdetail.data.model.AvailableFeature;
import com.vtt.chatlib.chatdetail.data.model.ConclusionMessage;
import com.vtt.chatlib.chatdetail.data.model.Message;
import com.vtt.chatlib.mbi.dto.BaseItemService;

import java.io.Serializable;
import java.util.ArrayList;

public class Response implements Serializable {

    @SerializedName("resolvedQuery")
    private String resolvedQuery;
    @SerializedName("tag")
    private String tag;
    @SerializedName("parameters")
    private ChatBotResponse.Parameters parameters;

    @SerializedName("messageAction")
    private Message.QuickReply messageAction;

    @SerializedName("chart")
    private BaseItemService chart;
    @SerializedName("speech")
    private String speech;
    @SerializedName("context")
    private Object context;

    @SerializedName("cardActions")
    private ArrayList<AvailableFeature> availableFeatures;

    @SerializedName("redirectAction")
    private ConclusionMessage conclusionMessage;
    @SerializedName("payload")
    private Object payload;

    public BaseItemService getChart() {
        return chart;
    }

    public void setChart(BaseItemService chart) {
        this.chart = chart;
    }

    private final static long serialVersionUID = -5205787623332599104L;

    public String getResolvedQuery() {
        return resolvedQuery;
    }

    public void setResolvedQuery(String resolvedQuery) {
        this.resolvedQuery = resolvedQuery;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public ChatBotResponse.Parameters getParameters() {
        return parameters;
    }

    public void setParameters(ChatBotResponse.Parameters parameters) {
        this.parameters = parameters;
    }

    public String getSpeech() {
        return speech;
    }

    public void setSpeech(String speech) {
        this.speech = speech;
    }

    public Message.QuickReply getMessageAction() {
        return messageAction;
    }

    public void setMessageAction(Message.QuickReply messageAction) {
        this.messageAction = messageAction;
    }

    public ArrayList<AvailableFeature> getAvailableFeatures() {
        return availableFeatures;
    }

    public void setAvailableFeatures(ArrayList<AvailableFeature> availableFeatures) {
        this.availableFeatures = availableFeatures;
    }

    public ConclusionMessage getConclusionMessage() {
        return conclusionMessage;
    }

    public void setConclusionMessage(ConclusionMessage conclusionMessage) {
        this.conclusionMessage = conclusionMessage;
    }

    public Object getContext() {
        return context;
    }

    public void setContext(Object context) {
        this.context = context;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Response{" +
                "resolvedQuery='" + resolvedQuery + '\'' +
                ", message=" + speech +
                ", tag='" + tag + '\'' +
                ", parameters=" + parameters +
                '}';
    }
}
