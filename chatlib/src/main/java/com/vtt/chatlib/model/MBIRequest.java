package com.vtt.chatlib.model;

/**
 * Created by HaiKE on 10/18/17.
 */

public class MBIRequest {

  ChatBotResponse.Parameters parameters;

  public ChatBotResponse.Parameters getParameters() {
    return parameters;
  }

  public void setParameters(ChatBotResponse.Parameters parameters) {
    this.parameters = parameters;
  }
}
