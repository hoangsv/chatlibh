package com.vtt.chatlib.model;

import com.google.gson.annotations.SerializedName;

class Subchannel {
    @SerializedName("type")
    private String type;
    @SerializedName("name")
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subchannel(String type, String name) {
        this.type = type;
        this.name = name;
    }
}
