package com.vtt.chatlib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BusinessUnit implements Serializable {

    @SerializedName("codes")
    @Expose
    private String codes;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("listUnitIds")
    @Expose
    private List<String> listUnitIds = null;
    private final static long serialVersionUID = -3955249932899715616L;

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getListUnitIds() {
        return listUnitIds;
    }

    public void setListUnitIds(List<String> listUnitIds) {
        this.listUnitIds = listUnitIds;
    }

    @Override
    public String toString() {
        return "BusinessUnit{" +
                "codes='" + codes + '\'' +
                ", type='" + type + '\'' +
                ", listUnitIds=" + listUnitIds +
                '}';
    }
}